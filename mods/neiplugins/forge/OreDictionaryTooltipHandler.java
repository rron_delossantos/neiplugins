package mods.neiplugins.forge;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

import codechicken.nei.forge.IContainerTooltipHandler;

import mods.neiplugins.common.TooltipCache;

public class OreDictionaryTooltipHandler implements IContainerTooltipHandler
{
	public static TooltipCache tooltipCache = new TooltipCache(true);
	public static boolean isEnabled = false;

	@Override
	public List<String> handleTooltipFirst(GuiContainer gui, int mousex, int mousey, List<String> currenttip)
	{
		return currenttip;
	}

	public static void resetCache()
	{
		tooltipCache.clear();
		String[] oreNames = OreDictionary.getOreNames();
		Arrays.sort(oreNames);

		for (String oreName : oreNames) {
			int oreID = OreDictionary.getOreID(oreName);
			for (ItemStack ore : OreDictionary.getOres(oreName))
			{
				if (ore.getItemDamage() != OreDictionary.WILDCARD_VALUE)
					continue;

				List<String> tooltip = new ArrayList<String>();
				tooltipCache.addCachedTooltip(ore, tooltip);
				if (tooltip.size()==0)
					tooltip.add("\u00a77"+"Ore Dictionary names:");

				int mainOreID = OreDictionary.getOreID(ore);
				if (mainOreID == oreID)
					tooltip.add("\u00a77"+oreName);
				else
					tooltip.add("\u00a78"+oreName);
				tooltipCache.put(ore, tooltip);
			}
		}

		for (String oreName : oreNames) {
			int oreID = OreDictionary.getOreID(oreName);
			for (ItemStack ore : OreDictionary.getOres(oreName))
			{
				if (ore.getItemDamage() == OreDictionary.WILDCARD_VALUE)
					continue;

				List<String> tooltip = new ArrayList<String>();
				tooltipCache.addCachedTooltip(ore, tooltip);
				if (tooltip.size()==0)
					tooltip.add("\u00a77"+"Ore Dictionary names:");

				int mainOreID = OreDictionary.getOreID(ore);
				if (mainOreID == oreID) {
					for (int i=1; i< tooltip.size(); i++)
					{
						if (tooltip.get(i).startsWith("\u00a77") && !tooltip.get(i).equals("\u00a77"+oreName))
							tooltip.add(i,"\u00a78"+tooltip.remove(i).substring(2));
					}
					tooltip.add("\u00a77"+oreName);
				} else
					tooltip.add("\u00a78"+oreName);

				tooltipCache.put(ore, tooltip);
			}
		}
	}

	@Override
	public List<String> handleItemTooltip(GuiContainer gui, ItemStack stack, List<String> currenttip) {
		if (stack == null || !isEnabled)
			return currenttip;

		if (tooltipCache.addCachedWildcardTooltip(stack, currenttip))
			return currenttip;
		return currenttip;
	}

}
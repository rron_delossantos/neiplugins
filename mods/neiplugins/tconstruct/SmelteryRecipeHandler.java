package mods.neiplugins.tconstruct;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.liquids.LiquidStack;

import codechicken.nei.NEIClientUtils;
import codechicken.nei.PositionedStack;
import codechicken.nei.forge.GuiContainerManager;

import mods.neiplugins.common.ICachedRecipeWithLiquidTank;
import mods.neiplugins.common.LiquidHelper;
import mods.neiplugins.common.LiquidTank;
import mods.neiplugins.common.LiquidTemplateRecipeHandler;
import mods.tinker.tconstruct.library.crafting.Smeltery;

public class SmelteryRecipeHandler extends LiquidTemplateRecipeHandler
{
	public static final int xOffset = 0;
	static Rectangle metalTank = new Rectangle(xOffset + 116-1,23-1,18,18);

	public class CachedSmelteryRecipe extends CachedRecipe implements ICachedRecipeWithLiquidTank
	{
		PositionedStack input;
		LiquidTank metal;
		Integer temperature;

		public CachedSmelteryRecipe(Integer blockID, Integer meta)
		{
			temperature = Smeltery.getLiquifyTemperature(blockID, meta);

			metal = new LiquidTank(Smeltery.getSmelteryResult(blockID, meta), 1,metalTank);
			metal.showMillBuckets = true;
			metal.capacity = metal.liquid.amount;
			metal.showCapacity = false;
			input = new PositionedStack(new ItemStack(blockID, 1, meta), xOffset + 28, 21);
		}

		@Override
		public PositionedStack getResult()
		{
			return null;
		}

		@Override
		public PositionedStack getIngredient()
		{
			return input;
		}

		@Override
		public ArrayList<LiquidTank> getLiquidTanks()
		{
			ArrayList<LiquidTank> res = new ArrayList<LiquidTank>();
			res.add(metal);
			return res;
		}
	}

	@Override
	public String getRecipeName()
	{
		return "Smeltery";
	}

	@Override
	public String getRecipeId()
	{
		return "tconstruct.smeltery";
	}

	@Override
	public void loadTransferRects()
	{
		transferRects.add(new RecipeTransferRect(new Rectangle(xOffset + 72, 20, 16, 34), getRecipeId()));
	}

	@Override
	public void loadCraftingRecipes(String outputId, Object... results)
	{
		if(outputId.equals(getRecipeId()))
		{
			for(List<Integer> key : Smeltery.getSmeltingList().keySet())
			{
				arecipes.add(new CachedSmelteryRecipe(key.get(0), key.get(1)));
			}
		}
		else if (outputId.equals("liquid") && results.length == 1 && results[0] instanceof LiquidStack)
		{
			LiquidStack t = (LiquidStack)results[0];
			for(Map.Entry<List<Integer>, LiquidStack> pair : Smeltery.getSmeltingList().entrySet())
			{
				if(pair.getValue().isLiquidEqual(t))
					arecipes.add(new CachedSmelteryRecipe(pair.getKey().get(0), pair.getKey().get(1)));
			}
		}
		else
		{
			super.loadCraftingRecipes(outputId, results);
		}
	}

	public void loadUsageRecipes(ItemStack ingred)
	{
		for(List<Integer> key : Smeltery.getSmeltingList().keySet())
		{
			if(NEIClientUtils.areStacksSameTypeCrafting(new ItemStack(key.get(0), 1, key.get(1)), ingred))
			{
				arecipes.add(new CachedSmelteryRecipe(key.get(0), key.get(1)));
			}
		}
	}

	public void loadCraftingRecipes(ItemStack result)
	{
		LiquidStack t = LiquidHelper.getLiquidStack(result);
		if (t != null)
		{
			for(Map.Entry<List<Integer>, LiquidStack> pair : Smeltery.getSmeltingList().entrySet())
			{
				if(pair.getValue().isLiquidEqual(t))
					arecipes.add(new CachedSmelteryRecipe(pair.getKey().get(0), pair.getKey().get(1)));
			}
		}
	}

	@Override
	public void drawBackground(GuiContainerManager gui, int recipe)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		gui.bindTexture(getGuiTexture());
		gui.drawTexturedModalRect(xOffset, 0, 0, 0, 160, 55);
	}

	@Override
	public String getGuiTexture()
	{
		return "/mods/neiplugins/gfx/tc_smeltery.png";
	}

	@Override
	public void drawExtras(GuiContainerManager gui, int recipe)
	{
		Integer temperature = ((CachedSmelteryRecipe)arecipes.get(recipe)).temperature;
		String tempString = temperature.toString()+" C";
		gui.drawTextCentered(tempString, xOffset+81, 9, 0xFF808080, false);

	}
}
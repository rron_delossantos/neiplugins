package mods.neiplugins.tconstruct;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.liquids.LiquidStack;

import codechicken.nei.PositionedStack;
import codechicken.nei.forge.GuiContainerManager;

import mods.neiplugins.common.ICachedRecipeWithLiquidTank;
import mods.neiplugins.common.LiquidHelper;
import mods.neiplugins.common.LiquidTank;
import mods.neiplugins.common.LiquidTemplateRecipeHandler;
import mods.tinker.tconstruct.library.crafting.AlloyMix;
import mods.tinker.tconstruct.library.crafting.Smeltery;

public class AlloySmelteryRecipeHandler extends LiquidTemplateRecipeHandler
{
	public static final int xOffset = 0;
	static Rectangle outputTank = new Rectangle(xOffset + 119-1,48-32-7,18,32);

	public class CachedAlloyMixRecipe extends CachedRecipe implements ICachedRecipeWithLiquidTank
	{
		ArrayList<LiquidTank> tanks;
		int minAmount;

		public CachedAlloyMixRecipe(AlloyMix recipe)
		{
			tanks = new ArrayList<LiquidTank>();

			int maxAmount = recipe.mixers.get(0).amount;
			int mult = 1;
			minAmount = maxAmount;

			for (LiquidStack stack : recipe.mixers)
			{
				if (stack.amount > maxAmount)
					maxAmount = stack.amount;
				if (stack.amount < minAmount)
					minAmount = stack.amount;
			}
			//if (minAmount < 1000)
			//	mult = (1000/minAmount);
			//if (minAmount*mult < 1000)
			//	mult += 1;

			LiquidTank tank = new LiquidTank(recipe.result, maxAmount*mult,outputTank);
			tank.showMillBuckets = true;
			tank.liquid.amount *= mult;
			tank.showCapacity = false;
			tanks.add(tank);

			int width = 36 / recipe.mixers.size();//36 5 7
			int counter = 0;
			for (LiquidStack stack : recipe.mixers)
			{
				if (counter == recipe.mixers.size()-1)
					tank = new LiquidTank(stack, maxAmount*mult, new Rectangle(xOffset+21+width*counter, 48-32-7, 36-width*counter, 32));
				else
					tank = new LiquidTank(stack, maxAmount*mult, new Rectangle(xOffset+21+width*counter, 48-32-7, width, 32));
				//tank.capacity = tank.liquid.amount;
				tank.showMillBuckets = true;
				tank.liquid.amount *= mult;
				tank.showCapacity = false;
				tanks.add(tank);
				counter += 1;
			}
		}

		@Override
		public PositionedStack getResult()
		{
			return null;
		}

		@Override
		public PositionedStack getIngredient()
		{
			return null;
		}

		@Override
		public ArrayList<LiquidTank> getLiquidTanks()
		{
			return tanks;
		}
	}

	@Override
	public String getRecipeName()
	{
		return "Alloy Smeltery";
	}

	@Override
	public String getRecipeId()
	{
		return "tconstruct.alloysmeltery";
	}

	@Override
	public void loadTransferRects()
	{
		transferRects.add(new RecipeTransferRect(new Rectangle(xOffset + 76, 28-7, 22, 15), getRecipeId()));
	}

	private boolean isValidRecipe(AlloyMix recipe)
	{
		return !recipe.mixers.isEmpty();
	}
	@Override
	public void loadCraftingRecipes(String outputId, Object... results)
	{
		if(outputId.equals(getRecipeId()))
		{
			for(AlloyMix recipe : Smeltery.getAlloyList())
			{
				if (isValidRecipe(recipe))
					arecipes.add(new CachedAlloyMixRecipe(recipe));
			}
		}
		else if (outputId.equals("liquid") && results.length == 1 && results[0] instanceof LiquidStack)
		{
			LiquidStack t = (LiquidStack)results[0];
			for(AlloyMix recipe : Smeltery.getAlloyList())
			{
				if(recipe.result.isLiquidEqual(t) && isValidRecipe(recipe))
					arecipes.add(new CachedAlloyMixRecipe(recipe));
			}
		}
		else
		{
			super.loadCraftingRecipes(outputId, results);
		}
	}

	@Override
	public void loadUsageRecipes(String inputId, Object... ingredients)
	{
		if (inputId.equals("liquid") && ingredients.length == 1 && ingredients[0] instanceof LiquidStack)
		{
			LiquidStack t = (LiquidStack)ingredients[0];
			for(AlloyMix recipe : Smeltery.getAlloyList())
			{
				for (LiquidStack liquid : recipe.mixers)
				{
					if(liquid.isLiquidEqual(t) && isValidRecipe(recipe))
					{
						arecipes.add(new CachedAlloyMixRecipe(recipe));
						break;
					}
				}
			}
		}
		else
		{
			super.loadUsageRecipes(inputId, ingredients);
		}
	}

	public void loadUsageRecipes(ItemStack ingred)
	{
		LiquidStack t = LiquidHelper.getLiquidStack(ingred);
		if (t != null)
		{
			for(AlloyMix recipe : Smeltery.getAlloyList())
			{
				for (LiquidStack liquid : recipe.mixers)
				{
					if(liquid.isLiquidEqual(t) && isValidRecipe(recipe))
					{
						arecipes.add(new CachedAlloyMixRecipe(recipe));
						break;
					}
				}
			}
		}
	}

	public void loadCraftingRecipes(ItemStack result)
	{
		LiquidStack t = LiquidHelper.getLiquidStack(result);
		if (t != null)
		{
			for(AlloyMix recipe : Smeltery.getAlloyList())
			{
				if(recipe.result.isLiquidEqual(t) && isValidRecipe(recipe))
					arecipes.add(new CachedAlloyMixRecipe(recipe));
			}
		}
	}

	@Override
	public void drawBackground(GuiContainerManager gui, int recipe)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		gui.bindTexture(getGuiTexture());
		gui.drawTexturedModalRect(xOffset, 0, 0, 54+8, 160, 65);
	}

	@Override
	public String getGuiTexture()
	{
		return "/mods/neiplugins/gfx/tc_smeltery.png";
	}
}
package mods.neiplugins.mfr;

import java.util.HashMap;
import java.util.List;

import net.minecraftforge.liquids.LiquidDictionary;
import net.minecraftforge.liquids.LiquidStack;

import mods.neiplugins.common.FuelTooltip;
import mods.neiplugins.common.SimpleFuelContextHelper;
import mods.neiplugins.common.SimpleFuelHelper;
import mods.neiplugins.common.Utils;

public class MFRFuelHelper {

	public static Class GuiBioFuelGenerator;
	public static int bfgLiquidConsumedPerTick;
	public static int bfgEnergyProducedPerConsumption;
	public static int bfgTicksBetweenConsumption;

	public static void registerFuelHelpers()
	{
		FuelTooltip.addFuelHelper(new SimpleFuelHelper("mfr.bfg", "BioFuel Generator", null) {
			@Override
			public List<String> getLiquidStackFuelTooltip(LiquidStack liquid, List<String> currenttip)
			{
				if (LiquidDictionary.findLiquidName(liquid).equals("biofuel"))
					currenttip.add(FuelTooltip.linePrefix + FuelTooltip.convertMJtF(1.f*bfgEnergyProducedPerConsumption/(bfgTicksBetweenConsumption+1)) + " MJ/t for " + liquid.amount*(bfgTicksBetweenConsumption+1)/bfgLiquidConsumedPerTick+" ticks (BioFuel Generator)");
				return currenttip;
			}
		});

		GuiBioFuelGenerator = Utils.findClass("powercrystals.minefactoryreloaded.gui.client.GuiBioFuelGenerator");

		HashMap<Class, String> map = new HashMap<Class, String>();
		if (GuiBioFuelGenerator != null)
			map.put(GuiBioFuelGenerator, "mfr.bfg");
		if (!map.isEmpty())
			FuelTooltip.addContextFuelHelper(new SimpleFuelContextHelper("MFR.all","MFR",null,map));

	}


}
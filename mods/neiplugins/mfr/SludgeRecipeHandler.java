package mods.neiplugins.mfr;

import java.awt.Rectangle;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.WeightedRandom;
import net.minecraft.util.WeightedRandomItem;
import net.minecraftforge.liquids.LiquidDictionary;
import net.minecraftforge.liquids.LiquidStack;

import codechicken.nei.NEIClientUtils;
import codechicken.nei.PositionedStack;
import codechicken.nei.forge.GuiContainerManager;
import codechicken.nei.recipe.TemplateRecipeHandler;

import mods.neiplugins.common.ICachedRecipeWithLiquidTank;
import mods.neiplugins.common.LiquidHelper;
import mods.neiplugins.common.LiquidTank;
import mods.neiplugins.common.LiquidTemplateRecipeHandler;
import mods.neiplugins.common.Utils;
import powercrystals.core.random.WeightedRandomItemStack;
import powercrystals.minefactoryreloaded.MFRRegistry;

public class SludgeRecipeHandler extends LiquidTemplateRecipeHandler
{
	public static Rectangle inputTank = new Rectangle (42,20,24,24);
	public static LiquidTank input = null;
	public class CachedSludgeRecipe extends CachedRecipe implements ICachedRecipeWithLiquidTank
	{
		public PositionedStack result;
		public int weight;

		public CachedSludgeRecipe(WeightedRandomItem entry)
		{
			result = new PositionedStack(((WeightedRandomItemStack)entry).getStack(), 111, 24);
			weight = entry.itemWeight;
		}

		public PositionedStack getIngredient()
		{
			return null;
		}

		public PositionedStack getResult()
		{
			return result;
		}

		public Float getChance()
		{
			if (totalWeight > 0)
				return 1.0f*weight/totalWeight;
			return 0.f;
		}

		@Override
		public ArrayList<LiquidTank> getLiquidTanks()
		{
			ArrayList<LiquidTank> res = new ArrayList<LiquidTank>();
			if (input != null)
				res.add(input);
			return res;
		}
	}

	public static DecimalFormat chanceFormat = new DecimalFormat("0.##%");
	public static int totalWeight = 1;

	protected List<WeightedRandomItem> getRecipeList()
	{
		List<WeightedRandomItem> recipes = MFRRegistry.getSludgeDrops();
		totalWeight = WeightedRandom.getTotalWeight(recipes);
		return recipes;
	}

	public static boolean loadLiquid()
	{
		LiquidStack liquid = LiquidDictionary.getCanonicalLiquid("sludge");
		if (liquid == null)
			return false;
		input = new LiquidTank(liquid, 1000, inputTank);
		input.liquid.amount = 1000;
		input.showCapacity = false;
		return true;
	}

	@Override
	public String getRecipeName()
	{
		return "Sludge Boiler";
	}

	@Override
	public String getRecipeId()
	{
		return "mfr.sludge";
	}

	@Override
	public String getGuiTexture()
	{
		return "/mods/neiplugins/gfx/Liquid2Item.png";
	}
	@Override
	public void drawExtras(GuiContainerManager gui, int recipe)
	{
		Float chance = ((CachedSludgeRecipe)this.arecipes.get(recipe)).getChance();
		String chanceString = chanceFormat.format(chance);
		gui.drawTextCentered(chanceString, 85, 11, -8355712, false);
	}

	@Override
	public void loadTransferRects()
	{
		transferRects.add(new RecipeTransferRect(new Rectangle(74, 23, 25, 16), getRecipeId()));
	}

	@Override
	public void loadSameRecipeId()
	{
		for (WeightedRandomItem irecipe : getRecipeList())
		{
			if (irecipe instanceof WeightedRandomItemStack)
				arecipes.add(new CachedSludgeRecipe(irecipe));
		}
	}

	@Override
	public void loadCraftingRecipes(ItemStack result)
	{
		for (WeightedRandomItem irecipe : getRecipeList())
		{
			if (irecipe instanceof WeightedRandomItemStack && NEIClientUtils.areStacksSameTypeCrafting(((WeightedRandomItemStack)irecipe).getStack(), result))
				arecipes.add(new CachedSludgeRecipe(irecipe));
		}
	}

	@Override
	public void loadUsageRecipes(ItemStack ingredient)
	{
		LiquidStack t = LiquidHelper.getLiquidStack(ingredient);
		if (t != null)
			loadUsageRecipes(t);
	}
	@Override
	public void loadUsageRecipes(LiquidStack ingredient)
	{
		if (input != null && LiquidHelper.areSameLiquid(ingredient, input.liquid))
			loadSameRecipeId();
	}
}
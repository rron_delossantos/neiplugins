package mods.neiplugins;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Set;

import net.minecraft.block.Block;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import cpw.mods.fml.common.registry.GameRegistry;

import codechicken.nei.ItemRange;
import codechicken.nei.MultiItemRange;
import codechicken.nei.api.API;
import codechicken.nei.recipe.DefaultOverlayHandler;

import mods.neiplugins.common.IPlugin;
import mods.neiplugins.common.RecipeHandlerUtils;
import mods.neiplugins.common.Utils;
import mods.neiplugins.railcraft.BlastFurnaceRecipeHandler;
import mods.neiplugins.railcraft.CokeOvenRecipeHandler;
import mods.neiplugins.railcraft.RockCrusherRecipeHandler;
import mods.neiplugins.railcraft.RollingMachineRecipeHandler;
import mods.neiplugins.railcraft.RollingMachineShapelessRecipeHandler;
import mods.railcraft.api.core.items.IMinecartItem;
import mods.railcraft.api.core.items.ITrackItem;
import mods.railcraft.api.core.items.TagList;

public class NEIPlugins_Railcraft implements IPlugin
{
	public static final String PLUGIN_NAME = "Railcraft";
	public static final String PLUGIN_VERSION = "1.6.5";
	public static final String REQUIRED_MOD = "Railcraft";

	@Override
	public String getPluginName() {
		return PLUGIN_NAME;
	}

	@Override
	public String getPluginVersion() {
		return PLUGIN_VERSION;
	}

	@Override
	public boolean isValid() {
		return NEIPlugins.getMod().hasMod(REQUIRED_MOD);
	}

	@Override
	public void init()
	{
	}
	@Override
	public void loadConfig() {
		addHandlers();
		configureItemPanel();
	}

	private void addHandlers() {
		RockCrusherRecipeHandler.guiclass = (Class<? extends GuiContainer>) Utils.findClass("mods.railcraft.client.gui.GuiRockCrusher");
		BlastFurnaceRecipeHandler.guiclass = (Class<? extends GuiContainer>) Utils.findClass("mods.railcraft.client.gui.GuiBlastFurnace");
		CokeOvenRecipeHandler.guiclass = (Class<? extends GuiContainer>) Utils.findClass("mods.railcraft.client.gui.GuiCokeOven");
		RollingMachineRecipeHandler.guiclass = (Class<? extends GuiContainer>) Utils.findClass("mods.railcraft.client.gui.GuiRollingMachine");
		RollingMachineShapelessRecipeHandler.guiclass = (Class<? extends GuiContainer>) Utils.findClass("mods.railcraft.client.gui.GuiRollingMachine");

		API.registerGuiOverlay(RollingMachineRecipeHandler.guiclass, "railcraft.rolling");
		API.registerGuiOverlayHandler(RollingMachineRecipeHandler.guiclass, new DefaultOverlayHandler(), "railcraft.rolling");

		API.registerRecipeHandler(new RollingMachineRecipeHandler());
		API.registerUsageHandler(new RollingMachineRecipeHandler());
		API.registerRecipeHandler(new RollingMachineShapelessRecipeHandler());
		API.registerUsageHandler(new RollingMachineShapelessRecipeHandler());
		API.registerRecipeHandler(new CokeOvenRecipeHandler());
		API.registerUsageHandler(new CokeOvenRecipeHandler());
		API.registerRecipeHandler(new BlastFurnaceRecipeHandler());
		API.registerUsageHandler(new BlastFurnaceRecipeHandler());
		API.registerRecipeHandler(new RockCrusherRecipeHandler());
		API.registerUsageHandler(new RockCrusherRecipeHandler());

		RecipeHandlerUtils.addToRecipeList("Railcraft",getItemByTag("machine.alpha.rolling.machine"),"Rolling Machine",0,"railcraft.rolling");
		RecipeHandlerUtils.addToRecipeList("Railcraft",getItemByTag("machine.alpha.rock.crusher"),"Rock Crusher",0,"railcraft.crusher");
		RecipeHandlerUtils.addToRecipeList("Railcraft",getItemByTag("machine.alpha.coke.oven"),"Coke Oven",0,"railcraft.cokeoven");
		RecipeHandlerUtils.addToRecipeList("Railcraft",getItemByTag("machine.alpha.blast.furnace"),"Blast Furnace",0,"railcraft.blastfurnace");
	}

	private void configureItemPanel()
	{
		try{
			addSubsets();
	        } catch (Exception ex) {
			ex.printStackTrace();
        	}
	}
	private static ItemStack getItemByTag(String tag) {
		try
		{
			return GameRegistry.findItemStack("Railcraft",tag,1);
		}
		catch (Exception ex)
		{
			return null;
		}
	}

	private static void addTag(String tag, MultiItemRange items)
	{
		ItemStack item = getItemByTag(tag);
		if (item != null)
			items.add(item);
	}
	private static ArrayList<Entry<String,ItemStack>> getAllItems()
	{
		ArrayList<Entry<String,ItemStack>> result = new ArrayList<Entry<String,ItemStack>>();
		for (String tag : (Set<String>)TagList.getTags())
		{
			result.add(new SimpleEntry(tag, getItemByTag(tag)));
		}
		return result;
	}

	private void addSubsets()
	{
	 	MultiItemRange rails = new MultiItemRange();
		rails.add(Block.rail.blockID);
		rails.add(Block.railDetector.blockID);
		rails.add(Block.railPowered.blockID);

	 	MultiItemRange allcarts = new MultiItemRange();
	 	MultiItemRange allrails = new MultiItemRange();

		MultiItemRange woodenrails = new MultiItemRange();
		MultiItemRange speedrails = new MultiItemRange();
		MultiItemRange reinforcedrails = new MultiItemRange();
		MultiItemRange posts = new MultiItemRange();
		MultiItemRange walls = new MultiItemRange();
		MultiItemRange cubes = new MultiItemRange();
		MultiItemRange signals = new MultiItemRange();
		MultiItemRange detectors = new MultiItemRange();
		MultiItemRange machines = new MultiItemRange();
		MultiItemRange parts = new MultiItemRange();
		MultiItemRange tools = new MultiItemRange();

		MultiItemRange steeltools = new MultiItemRange();
		MultiItemRange armors = new MultiItemRange();


		MultiItemRange creosoteliquid = new MultiItemRange();
		MultiItemRange liquids = new MultiItemRange();
		MultiItemRange other = new MultiItemRange();
		MultiItemRange carts = new MultiItemRange();
		MultiItemRange dusts = new MultiItemRange();
		MultiItemRange ballasts = new MultiItemRange();

		carts.add(Item.minecartEmpty.itemID);
		carts.add(Item.minecartCrate.itemID);
		carts.add(Item.minecartPowered.itemID);

		for (Item i:Item.itemsList)
		{
			if (i instanceof ITrackItem)
			{
				allrails.add(i.itemID);
			}
			if (i instanceof IMinecartItem)
			{
				allcarts.add(i.itemID);
			}
		}

		for (Entry<String, ItemStack> entry: getAllItems())
		{
			if (entry.getKey().startsWith("track.slow"))
				woodenrails.add(entry.getValue());
			else if (entry.getKey().startsWith("track.speed"))
				speedrails.add(entry.getValue());
			else if (entry.getKey().startsWith("track.reinforced"))
				reinforcedrails.add(entry.getValue());
			else if (entry.getKey().startsWith("post"))
				posts.add(entry.getValue());
			else if (entry.getKey().startsWith("signal"))
				signals.add(entry.getValue());
			else if (entry.getKey().startsWith("detector."))
				detectors.add(entry.getValue());
			else if (entry.getKey().startsWith("machine."))
				machines.add(entry.getValue());
			else if (entry.getKey().startsWith("part."))
				parts.add(entry.getValue());
			else if (entry.getKey().startsWith("liquid.creosote."))
				creosoteliquid.add(entry.getValue());
			else if (entry.getKey().startsWith("liquid."))
				liquids.add(entry.getValue());
			else if (entry.getKey().startsWith("tool.steel."))
				steeltools.add(entry.getValue());
			else if (entry.getKey().startsWith("armor.steel."))
				armors.add(entry.getValue());
			else if (entry.getKey().startsWith("tool."))
				tools.add(entry.getValue());
			else if (entry.getKey().startsWith("dust."))
				dusts.add(entry.getValue());
			else if (entry.getKey().startsWith("cart."))
				carts.add(entry.getValue());
			else if (entry.getKey().startsWith("wall."))
				walls.add(entry.getValue());
			else if (entry.getKey().startsWith("cube."))
				cubes.add(entry.getValue());
			else if (!entry.getKey().startsWith("track."))
				other.add(entry.getValue());

			if (entry.getKey().startsWith("track."))
				rails.add(entry.getValue());

		}

		addTag("tool.signal.tuner", signals);
		addTag("tool.surveyor", signals);

		addTag("fuel.coke", other);
		addTag("ic2.upgrade.lapotron", other);

		/*addItemID("item.cart.tnt", carts);
		addItemID("item.cart.batbox", carts);
		addItemID("item.cart.mfe", carts);
		addItemID("item.cart.mfsu", carts);
		addItemID("item.cart.tank", carts);
		addItemID("item.cart.bore", carts);
		addItemID("item.cart.anchor", carts);
		addItemID("item.cart.work", carts);*/

		MultiItemRange crowbar = new MultiItemRange();
		addTag("tool.crowbar", crowbar);

		//API.addSetRange("RailCraft.All rails", allrails);
		//API.addSetRange("RailCraft.All carts", allcarts);

		API.addSetRange("RailCraft.Rails", rails);
		API.addSetRange("RailCraft.Rails.Wooden", woodenrails);
		API.addSetRange("RailCraft.Rails.HighSpeed", speedrails);
		API.addSetRange("RailCraft.Rails.Reinforced", reinforcedrails);
		API.addSetRange("RailCraft.Detectors", detectors);
		API.addSetRange("RailCraft.Structure.Post", posts);
		API.addSetRange("RailCraft.Structure.Walls", walls);
		API.addSetRange("RailCraft.Structure.Bricks", cubes);
		API.addSetRange("RailCraft.Carts", carts);
		API.addSetRange("RailCraft.Signals", signals);
		API.addSetRange("RailCraft.Parts", parts);
		API.addSetRange("RailCraft.Machines", machines);
		API.addSetRange("RailCraft.Tools", tools);
		API.addSetRange("RailCraft.Liquids.Creosote", creosoteliquid);
		API.addSetRange("RailCraft.Liquids", liquids);
		API.addSetRange("RailCraft.Other.Dusts", dusts);
		API.addSetRange("RailCraft.Other", other);
		API.addSetRange("RailCraft.Other.Armor", armors);
		API.addSetRange("RailCraft.Other.SteelTools", steeltools);
		API.addSetRange("RailCraft", crowbar);
		API.addToRange("Items.Tools.Other", crowbar);
	}

}
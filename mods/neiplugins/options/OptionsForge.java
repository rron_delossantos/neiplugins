package mods.neiplugins.options;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import codechicken.nei.GuiNEISettings.NEIOption;
import codechicken.nei.NEIClientConfig;
import codechicken.nei.api.API;
import codechicken.nei.recipe.GuiCraftingRecipe;

import mods.neiplugins.common.Registry;
import mods.neiplugins.forge.GuiGameRegistryCustomItemStackList;
import mods.neiplugins.forge.GuiGameRegistryModObjectTableList;
import mods.neiplugins.forge.OreDictionaryTooltipHandler;
import mods.neiplugins.lists.GuiList;
import mods.neiplugins.lists.IListElement;
import mods.neiplugins.lists.SimpleListActiveElement;
import mods.neiplugins.lists.SimpleListElement;

public abstract class OptionsForge {
	public static void addOptions() {
		Registry.infoListMenu.add(new SimpleListElement("Liquid Dictionary") {
			@Override
			public boolean click(int button)
			{
				return button==0? GuiCraftingRecipe.openRecipeGui("forge.liquiddictionary"):false;
			}
		});

		Registry.developerListMenu.add(new SimpleListElement("Ore Dictionary") {
			@Override
			public boolean click(int button)
			{
				return button==0? GuiCraftingRecipe.openRecipeGui("forge.oredictionary"):false;
			}
		});

		Registry.developerListMenu.add(new SimpleListElement("Forge Game Registry") {
			@Override
			public boolean click(int button)
			{
				return button==0?GuiList.showList(this.title,forgeDeveloperListMenu):false;
			}
		});

		Registry.developerListMenu.add(new SimpleListActiveElement("Ore Dictionary tooltip", false) {
			@Override
			public boolean isActive()
			{
				return OreDictionaryTooltipHandler.isEnabled;
			}

			@Override
			public boolean click(int button)
			{
				if (button == 0)
					OreDictionaryTooltipHandler.isEnabled = !OreDictionaryTooltipHandler.isEnabled;
				return button==0;
			}
		});

		forgeDeveloperListMenu.add(new SimpleListElement("Custom Item Stack") {
			@Override
			public boolean click(int button)
			{
				return button==0?GuiGameRegistryCustomItemStackList.showList(this.title):false;
			}
		});

		forgeDeveloperListMenu.add(new SimpleListElement("Mod Object Table") {
			@Override
			public boolean click(int button)
			{
				return button==0?GuiGameRegistryModObjectTableList.showList(this.title):false;
			}
		});

		NEIClientConfig.globalConfig.getTag("mistaqur.showLiquidDictionaryNames").setDefaultValue("true");
	    	API.addSetting(GuiNEIPluginsMain.class, new NEIOption("mistaqur.showLiquidDictionaryNames")
			{
				@Override
				public String updateText()
				{
					return ""+(enabled() ? "Show Liquid Dictionary names" : "Disable Liquid Dictionary names");
				}
				public void onClick()
				{
					super.onClick();
				}
			});

		NEIClientConfig.globalConfig.getTag("mistaqur.showUsageInOreDictionary").setDefaultValue("true");
	    	API.addSetting(GuiNEIPluginsMain.class, new NEIOption("mistaqur.showUsageInOreDictionary")
			{
				@Override
				public String updateText()
				{
					return ""+(enabled() ? "Enable Ore Dictionary" : "Disable Ore Dictionary");
				}
				public void onClick()
				{
					super.onClick();
				}
			});
	}

	public static ArrayList<IListElement> forgeDeveloperListMenu = new ArrayList<IListElement>();
}
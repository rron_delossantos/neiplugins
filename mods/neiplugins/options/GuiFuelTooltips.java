package mods.neiplugins.options;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;

import codechicken.nei.NEIClientUtils;

import mods.neiplugins.common.FuelTooltip;
import mods.neiplugins.lists.GuiButtonList;
import mods.neiplugins.lists.GuiList;
import mods.neiplugins.lists.IActiveListElement;
import mods.neiplugins.lists.IListElement;
import mods.neiplugins.lists.SimpleListElement;

public class GuiFuelTooltips extends GuiButtonList
{
	protected GuiFuelTooltips(GuiContainer prevgui, String title, ArrayList<? extends IListElement> list)
	{
		super(prevgui, title, list);
	}

	static class ListElement1 extends SimpleListElement implements IActiveListElement
	{
		int index;
		ListElement1(int index)
		{
			super(""+index);
			this.index = index;
		}

		@Override
		public boolean click(int button)
		{
			if (button !=0)return false;
			String key = FuelTooltip.helpersPosition.get(index);
			if (FuelTooltip.disabledHelpers.contains(key))
				FuelTooltip.disabledHelpers.remove(key);
			else
				FuelTooltip.disabledHelpers.add(key);
			FuelTooltip.updateSettings();
			return true;
		}

		@Override
		public List<String> handleTooltip(GuiList gui, List<String> currenttip, Point mousepos)
		{
			List<String> t = FuelTooltip.registredHelpers.get(FuelTooltip.helpersPosition.get(index)).getDescription();
			if (t != null)
				currenttip.addAll(t);
			return currenttip;
		}

		@Override
		public String getTitle()
		{
			return FuelTooltip.registredHelpers.get(FuelTooltip.helpersPosition.get(index)).getName();
		}
		@Override
		public boolean isActive()
		{
			return !FuelTooltip.disabledHelpers.contains(FuelTooltip.helpersPosition.get(index));
		}

	}

	public static boolean showList(String title)
	{
		Minecraft mc = NEIClientUtils.mc();
		if(!(mc.currentScreen instanceof GuiContainer))
			return false;
		GuiContainer prevscreen = (GuiContainer) mc.currentScreen;

		ArrayList<IListElement> list = new ArrayList<IListElement>();
		for (int i=0; i< FuelTooltip.helpersPosition.size();i++)
		{
			list.add(new ListElement1(i));
		}

		if(list.isEmpty())
			return false;

		NEIClientUtils.overlayScreen(new GuiFuelTooltips(prevscreen, title, list));
		return true;
	}
}
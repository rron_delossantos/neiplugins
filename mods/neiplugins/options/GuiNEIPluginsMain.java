package mods.neiplugins.options;

import net.minecraft.client.gui.inventory.GuiContainer;

import codechicken.nei.GuiNEISettings;

public class GuiNEIPluginsMain extends GuiNEISettings
{
	public GuiNEIPluginsMain(GuiContainer parentContainer)
	{
		super(parentContainer);
	}

	@Override
	public String getBackButtonName()
	{
		return "Settings";
	}

	@Override
	public void onBackButtonClick()
	{
		mc.displayGuiScreen(new GuiNEISettings(parentScreen));
	}

	@Override
	public void updateScreen()
	{
		super.updateScreen();

		updateButtonNames();
	}
}
package mods.neiplugins;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;

import codechicken.nei.api.API;
import codechicken.nei.forge.GuiContainerManager;

import mods.neiplugins.common.IPlugin;
import mods.neiplugins.lists.IListElement;
import mods.neiplugins.lists.NEIPluginsInputHandler;
import mods.neiplugins.options.OptionsLists;

public class NEIPlugins_Lists implements IPlugin {

	public static final String PLUGIN_NAME = "Lists";
	public static final String PLUGIN_VERSION = "1.0.1";
	public static final String REQUIRED_MOD = "";

	@Override
	public String getPluginName() {
		return PLUGIN_NAME;
	}

	@Override
	public String getPluginVersion() {
		return PLUGIN_VERSION;
	}

	@Override
	public boolean isValid() {
		return true;
	}

	@Override
	public void init() {
	}
	@Override
	public void loadConfig() {
		GuiContainerManager.addInputHandler(new NEIPluginsInputHandler());
		API.addKeyBind("mistaqur.recipeList", "Recipe Lists", Keyboard.KEY_L);
		mainMenu = new ArrayList<IListElement>();
		OptionsLists.addOptions();
	}

	public static ArrayList<IListElement> mainMenu;

}
package mods.neiplugins;

import codechicken.nei.api.API;

import mods.neiplugins.common.IPlugin;
import mods.neiplugins.common.RecipeHandlerUtils;
import mods.neiplugins.common.Registry;
import mods.neiplugins.common.Utils;
import mods.neiplugins.lists.SimpleListElement;
import mods.neiplugins.mfr.GuiLaserDrillList;
import mods.neiplugins.mfr.MFRFuelHelper;
import mods.neiplugins.mfr.SludgeRecipeHandler;

public class NEIPlugins_MFR implements IPlugin {

	public static final String PLUGIN_NAME = "MineFactoryReloaded";
	public static final String PLUGIN_VERSION = "1.0.3";
	public static final String REQUIRED_MOD = "MineFactoryReloaded";

	@Override
	public String getPluginName() {
		return PLUGIN_NAME;
	}

	@Override
	public String getPluginVersion() {
		return PLUGIN_VERSION;
	}

	@Override
	public boolean isValid() {
		if (!NEIPlugins.getMod().hasMod(REQUIRED_MOD))
			return false;
		Class MFRRegistry = Utils.findClass("powercrystals.minefactoryreloaded.MFRRegistry");
		if (Utils.findClass("powercrystals.core.random.WeightedRandomItemStack") == null  || MFRRegistry == null) {
			NEIPlugins.logInfo("Failed to find necessary classes for mod \"{0}\"",REQUIRED_MOD);
			return false;
		}
		sludgeActive = Utils.findMethod(MFRRegistry,"getSludgeDrops") != null;
		laserDrillActive = Utils.findMethod(MFRRegistry,"getLaserOres") != null;
		return true;
	}

	@Override
	public void init() {
	}

	@Override
	public void loadConfig() {
		if (sludgeActive && SludgeRecipeHandler.loadLiquid())
		{
			RecipeHandlerUtils.addToRecipeList("MineFactory Reloaded","Sludge Boiler",0,"mfr.sludge");
			API.registerUsageHandler(new SludgeRecipeHandler());
			API.registerRecipeHandler(new SludgeRecipeHandler());
		} else {
			NEIPlugins.logInfo("Sludge Boiler recipe handler is disabled");
		}


		if (laserDrillActive)
		{
			Registry.infoListMenu.add(new SimpleListElement("MFR Laser Drill Drops") {
				@Override
				public boolean click(int button)
				{
					return button==0?GuiLaserDrillList.showList(this.title):false;
				}
			});
		} else {
			NEIPlugins.logInfo("Laser Drill info list is disabled");
		}
		Class cls = Utils.findClass("powercrystals.minefactoryreloaded.tile.machine.TileEntityBioFuelGenerator");
		MFRFuelHelper.bfgEnergyProducedPerConsumption = Utils.getFieldInt(cls, "energyProducedPerConsumption", null);
		if (MFRFuelHelper.bfgEnergyProducedPerConsumption > 0) {
			MFRFuelHelper.bfgLiquidConsumedPerTick = Utils.getFieldInt(cls, "liquidConsumedPerTick", null);
			MFRFuelHelper.bfgTicksBetweenConsumption = Utils.getFieldInt(cls, "ticksBetweenConsumption", null);
			if (MFRFuelHelper.bfgLiquidConsumedPerTick > 0)
				MFRFuelHelper.registerFuelHelpers();
		}
	}
	private boolean sludgeActive;
	private boolean laserDrillActive;

}
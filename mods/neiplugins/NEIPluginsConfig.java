package mods.neiplugins;

import codechicken.nei.api.IConfigureNEI;

public class NEIPluginsConfig implements IConfigureNEI
{
	@Override
	public void loadConfig()
	{
		NEIPlugins.getMod().loadConfig();
	}

	@Override
	public String getName()
	{
		return "NEIPlugins";
	}

	public String getVersion()
	{
		return NEIPlugins.VERSION;
	}
}
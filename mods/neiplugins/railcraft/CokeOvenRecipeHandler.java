package mods.neiplugins.railcraft;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.liquids.LiquidStack;

import codechicken.nei.NEIClientConfig;
import codechicken.nei.NEIClientUtils;
import codechicken.nei.PositionedStack;
import codechicken.nei.forge.GuiContainerManager;
import codechicken.nei.recipe.FurnaceRecipeHandler;
import codechicken.nei.recipe.GuiCraftingRecipe;
import codechicken.nei.recipe.GuiRecipe;
import codechicken.nei.recipe.GuiUsageRecipe;

import mods.neiplugins.common.ICachedRecipeWithLiquidTank;
import mods.neiplugins.common.LiquidHelper;
import mods.neiplugins.common.LiquidTank;
import mods.railcraft.api.crafting.ICokeOvenRecipe;
import mods.railcraft.api.crafting.RailcraftCraftingManager;

public class CokeOvenRecipeHandler extends FurnaceRecipeHandler
{
	static Rectangle outputTank = new Rectangle (85,13,48,47);
	static Point overlayTank = new Point(176,0);

	public class CachedOvenRecipe extends CachedRecipe implements ICachedRecipeWithLiquidTank
	{
		ICokeOvenRecipe oven;
		ArrayList<PositionedStack> inputs;
		PositionedStack output;
		PositionedStack liquidInput;
		PositionedStack liquidOutput;
		LiquidTank tank;
		public CachedOvenRecipe(ICokeOvenRecipe base)
		{
			oven = base;
			tank = new LiquidTank(base.getLiquidOutput(),64000,outputTank,overlayTank);
			PositionedStack input = new PositionedStack(oven.getInput(), 11, 32);
			output = new PositionedStack(oven.getOutput(), 57, 32);
			PositionedStack[] output_cont = LiquidHelper.generateStacksForLiquid(tank.liquid,144,11,144,46);
			liquidInput = output_cont[0];
			liquidOutput = output_cont[1];

			inputs = new ArrayList<PositionedStack>();
			inputs.add(input);
		}

		@Override
		public PositionedStack getResult()
		{
			return output;
		}

		@Override
		public ArrayList<PositionedStack> getIngredients()
		{
			return getCycledIngredients(cycleticks / 20, inputs);
		}

		@Override
		public ArrayList<PositionedStack> getOtherStacks()
		{
			ArrayList<PositionedStack> stacks = new ArrayList<PositionedStack>();
			stacks.add(LiquidHelper.getSeqCycledStack(cycleticks / 20, liquidInput));
			stacks.add(LiquidHelper.getSeqCycledStack(cycleticks / 20, liquidOutput));
			return stacks;
		}

		@Override
		public ArrayList<LiquidTank> getLiquidTanks()
		{
			ArrayList<LiquidTank> res = new ArrayList<LiquidTank>();
			res.add(tank);
			return res;
		}
	}

	@Override
	public String getRecipeName()
	{
		return "Coke Oven";
	}

	@Override
	public void loadTransferRects()
	{
		transferRects.add(new RecipeTransferRect(new Rectangle(29, 32, 22, 16), "railcraft.cokeoven"));
	}

	public static Class<? extends GuiContainer> guiclass;

	@Override
	public Class<? extends GuiContainer> getGuiClass()
	{
		return guiclass;
	}

	@Override
	public void loadCraftingRecipes(String outputId, Object... results)
	{
		if(outputId.equals("railcraft.cokeoven") && getClass() == CokeOvenRecipeHandler.class)
		{
			for(ICokeOvenRecipe recipe : RailcraftCraftingManager.cokeOven.getRecipes())
			{
				arecipes.add(new CachedOvenRecipe(recipe));
			}
		}
		else if (outputId.equals("liquid") && results.length == 1 && results[0] instanceof LiquidStack)
		{
			LiquidStack t = (LiquidStack)results[0];
			for(ICokeOvenRecipe recipe : RailcraftCraftingManager.cokeOven.getRecipes())
			{
				if(recipe.getLiquidOutput().isLiquidEqual(t))
					arecipes.add(new CachedOvenRecipe(recipe));
			}

		}
		else
		{
			super.loadCraftingRecipes(outputId, results);
		}
	}

	public void loadUsageRecipes(ItemStack ingred)
	{
		for(ICokeOvenRecipe recipe : RailcraftCraftingManager.cokeOven.getRecipes())
		{
			if(NEIClientUtils.areStacksSameTypeCrafting(recipe.getInput(), ingred))
			{
				arecipes.add(new CachedOvenRecipe(recipe));
			}
		}
	}

	public void loadCraftingRecipes(ItemStack result)
	{
		LiquidStack t = LiquidHelper.getLiquidStack(result);
		for(ICokeOvenRecipe recipe : RailcraftCraftingManager.cokeOven.getRecipes())
		{
			if(t != null &&  recipe.getLiquidOutput().isLiquidEqual(t) || NEIClientUtils.areStacksSameType(result, recipe.getLiquidOutput().asItemStack()) || NEIClientUtils.areStacksSameType(result, recipe.getOutput()))
			{
				arecipes.add(new CachedOvenRecipe(recipe));
			}
		}
	}

	@Override
	public String getGuiTexture()
	{
		return "/mods/railcraft/textures/gui/gui_coke_oven.png";
	}


	@Override
	public void drawExtras(GuiContainerManager gui, int recipe)
	{

		//drawLiquidGauge(gui,85,13, 48, 47, lo.itemID, lo.amount, 64000);
		//drawLiquidGauge(   w,    h,        b,     90,     24,         48,          47, scaledOil, tile.getLiquidId());
		//drawLiquidGauge(guiX, guiY, guiWidth, gaugeX, gaugeY, gaugeWidth, gaugeHeight,     scale, liquidId)
		//drawProgressBar(gui, 43, 8, 176, 0, 14, 14, 48, 7);
		//drawProgressBar(gui, 66, 10, 176, 14, 24, 16, 48, 0);

        	//int steps = 1200 / ((CachedOvenRecipe) arecipes.get(recipe)).oven.getSecondaryProduction();//steps taken to produce
		//drawProgressBar(gui, 71, 36, 176, 28, 15, 24, 1/(float)steps * ((cycleticks/48) % steps+1), 3);
		drawLiquidTanks(gui, recipe);
	}

	// Common liquid code
	public void drawLiquidTanks(GuiContainerManager gui, int recipe)
	{
		CachedRecipe crecipe = arecipes.get(recipe);
		if (crecipe instanceof ICachedRecipeWithLiquidTank)
		{
			for (LiquidTank tank : ((ICachedRecipeWithLiquidTank)crecipe).getLiquidTanks())
			{
				tank.draw(gui, getGuiTexture());
			}
		}
	}

	protected boolean transferLiquidTank(GuiRecipe guiRecipe, int recipe, boolean usage)
	{
		CachedRecipe crecipe = arecipes.get(recipe);
		if (crecipe instanceof ICachedRecipeWithLiquidTank)
		{
			Point mousepos = guiRecipe.manager.getMousePosition();
			Point offset = guiRecipe.getRecipePosition(recipe);
			Point relMouse = new Point(mousepos.x - guiRecipe.guiLeft - offset.x, mousepos.y - guiRecipe.guiTop - offset.y);

			for (LiquidTank tank : ((ICachedRecipeWithLiquidTank)crecipe).getLiquidTanks())
			{
				if (tank.position.contains(relMouse))
				{
					if (tank.liquid.itemID > 0 && (usage ?
							GuiUsageRecipe.openRecipeGui("liquid", tank.liquid) :
							GuiCraftingRecipe.openRecipeGui("liquid", tank.liquid)))
						return true;
				}
			}
		}
		return false;
	}

	@Override
	public List<String> handleTooltip(GuiRecipe guiRecipe, List<String> currenttip, int recipe)
	{
		currenttip = super.handleTooltip(guiRecipe, currenttip, recipe);
		CachedRecipe crecipe = arecipes.get(recipe);
		if (guiRecipe.manager.shouldShowTooltip() && crecipe instanceof ICachedRecipeWithLiquidTank)
		{
			Point mousepos = guiRecipe.manager.getMousePosition();
			Point offset = guiRecipe.getRecipePosition(recipe);
			Point relMouse = new Point(mousepos.x - guiRecipe.guiLeft - offset.x, mousepos.y - guiRecipe.guiTop - offset.y);

			for (LiquidTank tank : ((ICachedRecipeWithLiquidTank)crecipe).getLiquidTanks())
			{
				if (tank.position.contains(relMouse))
				{
					tank.handleTooltip(currenttip);
				}
			}
		}
		return currenttip;
	}

	@Override
	public boolean keyTyped(GuiRecipe gui, char keyChar, int keyCode, int recipe)
	{
		if(keyCode == NEIClientConfig.getKeyBinding("recipe"))
		{
			if (transferLiquidTank(gui, recipe, false))
				return true;
		}
		else if(keyCode == NEIClientConfig.getKeyBinding("usage"))
		{
			if (transferLiquidTank(gui, recipe, true))
				return true;
		}
		return super.keyTyped(gui, keyChar, keyCode, recipe);
	}

	@Override
	public boolean mouseClicked(GuiRecipe gui, int button, int recipe)
	{
		if(button == 0)
		{
			if (transferLiquidTank(gui, recipe, false))
				return true;
		}
		else if(button == 1)
		{
			if (transferLiquidTank(gui, recipe, true))
				return true;
		}
		return super.mouseClicked(gui, button, recipe);
	}

}
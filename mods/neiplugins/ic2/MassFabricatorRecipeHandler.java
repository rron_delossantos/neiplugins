package mods.neiplugins.ic2;

import java.awt.Rectangle;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.ItemStack;

import codechicken.nei.NEIClientUtils;
import codechicken.nei.PositionedStack;
import codechicken.nei.forge.GuiContainerManager;
import codechicken.nei.recipe.TemplateRecipeHandler;

import ic2.api.recipe.Recipes;
import mods.neiplugins.NEIPlugins;
import mods.neiplugins.NEIPlugins_IC2;
import mods.neiplugins.common.Utils;

public class MassFabricatorRecipeHandler extends TemplateRecipeHandler
{
	public class CachedMassFabricatorRecipe extends CachedRecipe
	{
		public PositionedStack input;
		public int value;

		public PositionedStack getIngredient()
		{
			return input;
		}

		public PositionedStack getResult()
		{
			return matterPositionedStack;
		}

		public CachedMassFabricatorRecipe(Entry<ItemStack,Integer> entry)
		{
			value = 0;
			if (entry != null) {
				input = new PositionedStack(entry.getKey(), 109, 43);
				value = entry.getValue();
			}
		}
	}

	public static DecimalFormat countFormat = new DecimalFormat("0.##%");
	public static PositionedStack matterPositionedStack = null;

	public static Class<? extends GuiContainer> guiclass;

	@Override
	public Class<? extends GuiContainer> getGuiClass()
	{
		return guiclass;
	}

	@Override
	public String getRecipeName()
	{
		return "Mass Fabricator";
	}

	public String getRecipeId()
	{
		return "ic2.matter";
	}

	@Override
	public String getGuiTexture()
	{
		return "/mods/ic2/textures/gui/GUIMatter.png";
	}

	public void drawExtras(GuiContainerManager gui, int recipe)
	{
		int value = ((CachedMassFabricatorRecipe)arecipes.get(recipe)).value;
		//String costString = liquidAmountFormat.format(chance);
		if (true) {
			gui.drawText(11, 5, "Required:", 0xFF404040, false);
			if (value > 0) {
				int requiredEU = 166667;
				int requredItems = (requiredEU / value) + (requiredEU % value > 0?1:0);
				gui.drawText(11, 15, requiredEnergy+" EU", 0xFF404040, false);
				if (requredItems > 1)
					gui.drawText(11, 25, requredItems+" items", 0xFF404040, false);
				else if (requredItems == 1)
					gui.drawText(11, 25, requredItems+" item", 0xFF404040, false);
			} else {
				gui.drawText(11, 15, "1000000 EU", 0xFF404040, false);
			}
		}

		if (value > 0) {
			gui.drawText(11, 35, "Amplifier:", 0xFF404040, false);
			gui.drawText(11, 45, ""+value, 0xFF404040, false);
		}
	}

	@Override
	public void loadTransferRects()
	{
		transferRects.add(new RecipeTransferRect(new Rectangle(112, 28, 9, 14), getRecipeId()));
	}

	public static boolean addEmptyScrap = false;
	public static int requiredEnergy = 166667;

	protected Set<Entry<ItemStack,Integer>> getRecipeList()
	{
		if (matterPositionedStack == null && NEIPlugins_IC2.getIc2Item("matter") != null)
			matterPositionedStack = new PositionedStack(NEIPlugins_IC2.getIc2Item("matter"), 109, 7);
		return Recipes.matterAmplifier.getRecipes().entrySet();
	}

	@Override
	public void loadCraftingRecipes(String outputId, Object... results)
	{
		if(outputId.equals(getRecipeId()))
		{
			for (Entry<ItemStack,Integer> irecipe : getRecipeList())
			{
				arecipes.add(new CachedMassFabricatorRecipe(irecipe));
			}
		}
		else
		{
			super.loadCraftingRecipes(outputId, results);
		}
	}

	public void loadUsageRecipes(ItemStack ingredient)
	{
		for (Entry<ItemStack,Integer> irecipe : getRecipeList())
		{
			if (irecipe != null && NEIClientUtils.areStacksSameTypeCrafting(irecipe.getKey(), ingredient))
			{
				arecipes.add(new CachedMassFabricatorRecipe(irecipe));
			}
		}
	}

	public void loadCraftingRecipes(ItemStack result)
	{
		if (matterPositionedStack == null && NEIPlugins_IC2.getIc2Item("matter") != null)
			matterPositionedStack = new PositionedStack(NEIPlugins_IC2.getIc2Item("matter"), 109, 7);

		if (NEIClientUtils.areStacksSameTypeCrafting(matterPositionedStack.item, result)) {
			for (Entry<ItemStack,Integer> irecipe : getRecipeList())
			{
					arecipes.add(new CachedMassFabricatorRecipe(irecipe));
			}
		}
	}
}
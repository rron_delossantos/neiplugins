package mods.neiplugins.ic2;

import java.util.HashMap;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraftforge.liquids.LiquidContainerRegistry;
import net.minecraftforge.liquids.LiquidStack;

import ic2.api.info.Info;
import mods.neiplugins.common.FuelTooltip;
import mods.neiplugins.common.SimpleFuelContextHelper;
import mods.neiplugins.common.SimpleFuelHelper;
import mods.neiplugins.common.Utils;

public abstract class IC2FuelHelper {
	public static int energyGeneratorBase;
	public static int energyGeneratorGeo;
	public static void registerFuelHelpers()
	{
		Class IC2 = Utils.findClass("ic2.core.IC2");
		if (IC2 == null)
			return;

		energyGeneratorBase = Utils.getFieldInt(IC2, "energyGeneratorBase",null);
		energyGeneratorGeo = Utils.getFieldInt(IC2, "energyGeneratorGeo",null);

		if (energyGeneratorBase > 0)
			FuelTooltip.addFuelHelper(new SimpleFuelHelper("ic2.generator", "IC2 Generator", null) {
				@Override
				public List<String> getItemStackFuelTooltip(ItemStack stack, List<String> currenttip)
				{
					int energy = Info.itemFuel.getFuelValue(stack, false) / 4;
					if (energy > 0)
						currenttip.add(FuelTooltip.linePrefix + FuelTooltip.compactValue(energy * energyGeneratorBase) + " EU at "+energyGeneratorBase+" EU/t (Generator)");
					return currenttip;
				}
			});

		if (energyGeneratorGeo > 0)
			FuelTooltip.addFuelHelper(new SimpleFuelHelper("ic2.geothermal", "Geothermal Generator", null) {
				@Override
				public List<String> getLiquidStackFuelTooltip(LiquidStack liquid, List<String> currenttip)
				{
					if (liquid.itemID == Block.lavaStill.blockID)
						currenttip.add(FuelTooltip.linePrefix+FuelTooltip.compactValue(liquid.amount*energyGeneratorGeo)+" EU at "+energyGeneratorGeo+" EU/t (Geothermal Generator)");
					return currenttip;
				}
			});

		FuelTooltip.addFuelHelper(new SimpleFuelHelper("ic2.watermill", "Water Mill", null) {
			@Override
			public List<String> getItemStackFuelTooltip(ItemStack stack, List<String> currenttip)
			{
				LiquidStack liquid = LiquidContainerRegistry.getLiquidForFilledItem(stack);
				if (liquid != null && liquid.itemID == Block.waterStill.blockID ) {
					if (stack.getItem().hasContainerItem())
						currenttip.add(FuelTooltip.linePrefix+FuelTooltip.compactValue(500)+" EU at "+1+" EU/t (Water Mill)");
					else
						currenttip.add(FuelTooltip.linePrefix+FuelTooltip.compactValue(1000)+" EU at "+2+" EU/t (Water Mill)");
				}
				return currenttip;
			}
		});


		HashMap<Class, String> map = new HashMap<Class, String>();

		Class cls = Utils.findClass("ic2.core.block.generator.gui.GuiGenerator");
		if (cls != null && energyGeneratorBase > 0)
			map.put(cls, "ic2.generator");

		cls = Utils.findClass("ic2.core.block.generator.gui.GuiGeoGenerator");
		if (cls != null && energyGeneratorGeo > 0)
			map.put(cls, "ic2.geothermal");

		cls = Utils.findClass("ic2.core.block.generator.gui.GuiWaterGenerator");
		if (cls != null)
			map.put(cls, "ic2.watermill");

		if (!map.isEmpty())
			FuelTooltip.addContextFuelHelper(new SimpleFuelContextHelper("ic2.all", "IC2", null, map));


	}
}
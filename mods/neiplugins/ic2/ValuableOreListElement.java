package mods.neiplugins.ic2;

import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import net.minecraft.item.ItemStack;

import codechicken.nei.PositionedStack;

import mods.neiplugins.common.Utils;
import mods.neiplugins.lists.GuiList;
import mods.neiplugins.lists.MultipleStackListElement;

public class ValuableOreListElement extends MultipleStackListElement
{
	public ValuableOreListElement(Iterator<Entry<ItemStack,Integer>> stacks, int margin, Dimension size)
	{
		int pos = 1;
		this.margin = margin;
		this.stacks = new ArrayList<PositionedStack>();
		this.values = new ArrayList<Integer>();
		Entry<ItemStack,Integer> i;
		while ((pos + 16 < size.width) && stacks.hasNext()) {
			i = stacks.next();
			if (i.getKey() == null || !Utils.isSafeItemStack(i.getKey(), "mods.neiplugins.ic2.ValuableOreListElement"))
				continue;
			this.stacks.add(new PositionedStack(i.getKey(),pos,1));
			this.values.add(i.getValue());
			pos += 16 + margin;
		}
	}

	@Override
	protected List<String> handleItemTooltip(GuiList gui, int index, List<String> currenttip, Point mousePos)
	{
		currenttip.add("Value: "+values.get(index).toString());
		return currenttip;
	}

	public ArrayList<Integer> values;
}
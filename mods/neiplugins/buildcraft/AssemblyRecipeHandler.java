package mods.neiplugins.buildcraft;

import java.awt.Rectangle;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Container;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

import codechicken.nei.NEIClientUtils;
import codechicken.nei.PositionedStack;
import codechicken.nei.forge.GuiContainerManager;
import codechicken.nei.recipe.RecipeInfo;
import codechicken.nei.recipe.TemplateRecipeHandler;

import buildcraft.api.recipes.AssemblyRecipe;

public class AssemblyRecipeHandler extends TemplateRecipeHandler
{
	static final int[][] stackorder = new int[][]{
		{0,0},
		{1,0},
		{0,1},
		{1,1},
		{0,2},
		{1,2},
		{2,0},
		{2,1},
		{2,2}};
	public static LinkedList assemblyRecipes;

	public static Class assemblyRecipeClass;
	public static Field inputField;
	public static Field outputField;
	public static Field energyField;

	public class CachedAssemblyRecipe extends CachedRecipe
	{
		public CachedAssemblyRecipe(AssemblyRecipe recipe)
		{
			result = new PositionedStack(recipe.output, 119, 24);
			ingredients = new ArrayList<PositionedStack>();
			setIngredients(Arrays.asList(recipe.input));
			this.energy = recipe.energy;
		}

		public CachedAssemblyRecipe(ItemStack[] input, ItemStack output, float energy)
		{
			this.result = new PositionedStack(output, 119, 24);
			this.ingredients = new ArrayList<PositionedStack>();
			setIngredients(Arrays.asList(input));
			this.energy = energy;
		}

		public void setIngredients(List<ItemStack> items)
		{
			for(int ingred = 0; ingred < items.size(); ingred++)
			{
				ItemStack ingredient = items.get(ingred);
				PositionedStack stack = new PositionedStack(ingredient, 25 + stackorder[ingred][0] * 18, 6 + stackorder[ingred][1] * 18);
				ingredients.add(stack);
			}
		}

		public void replaceMetaOnIngredients(ItemStack ingredient)
		{

			for(PositionedStack stack : ingredients)
			{
				if(stack.item.getItemDamage() == OreDictionary.WILDCARD_VALUE && NEIClientUtils.areStacksSameTypeCrafting(ingredient, stack.item))
				{
					stack.item.setItemDamage(ingredient.getItemDamage());
				}
			}
		}

		@Override
		public ArrayList<PositionedStack> getIngredients()
		{
			int cycle = cycleticks / 20;
			ArrayList<PositionedStack> cycledstacks = new ArrayList<PositionedStack>();
			for(PositionedStack basestack : ingredients)
			{
				PositionedStack stack = basestack.copy();
				if(stack.item.getItemDamage() == OreDictionary.WILDCARD_VALUE)
				{
					int maxDamage = 0;
					do
					{
						maxDamage++;
						stack.item.setItemDamage(maxDamage);
					}
					while(NEIClientUtils.isValidItem(stack.item));

					stack.item.setItemDamage(cycle % maxDamage);
				}
				cycledstacks.add(stack);
			}
			return cycledstacks;
		}

		@Override
		public PositionedStack getResult()
		{
			return result;
		}

		ArrayList<PositionedStack> ingredients;
		PositionedStack result;
		float energy;
	}

	@Override
	public String getRecipeName()
	{
		return "Assembly Table";
	}

	@Override
	public boolean hasOverlay(GuiContainer gui, Container container, int recipe)
	{
		return RecipeInfo.hasDefaultOverlay(gui, getOverlayIdentifier()) || RecipeInfo.hasOverlayHandler(gui, getOverlayIdentifier());
	}

	@Override
	public String getOverlayIdentifier()
	{
		return "buildcraft.assemblyTable";
	}

	@Override
	public String getGuiTexture()
	{
		return "/gui/crafting.png";
	}

	@Override
	public void drawExtras(GuiContainerManager gui, int recipe)
	{
		//DecimalFormat df = new DecimalFormat("#0.#");
		//String costString = df.format(((CachedAssemblyRecipe)arecipes.get(recipe)).energy/1000) + " t";
		Integer time = Math.round(((CachedAssemblyRecipe)arecipes.get(recipe)).energy);
		String costString = time.toString();
		gui.drawTextCentered(costString, 110, 9, 0xFF808080, false);
	}

	@Override
	public void loadTransferRects()
	{
		transferRects.add(new RecipeTransferRect(new Rectangle(84, 23, 24, 18), "buildcraft.assembly"));
	}

	@Override
	public void loadCraftingRecipes(String outputId, Object... results)
	{
		if(outputId.equals("buildcraft.assembly") && getClass() == AssemblyRecipeHandler.class)
		{
		    	for(AssemblyRecipe irecipe : AssemblyRecipe.assemblyRecipes)
			{
				arecipes.add(new CachedAssemblyRecipe(irecipe));
			}
		}
		else
		{
			super.loadCraftingRecipes(outputId, results);
		}
	}

	@Override
	public void loadCraftingRecipes(ItemStack result)
	{
	    	for(AssemblyRecipe irecipe : AssemblyRecipe.assemblyRecipes)
		{
			if (NEIClientUtils.areStacksSameTypeCrafting(result,irecipe.output)) {
				arecipes.add(new CachedAssemblyRecipe(irecipe));
			}
		}
	}

	public boolean recipeContain(AssemblyRecipe irecipe, ItemStack ingredient) {
		for (ItemStack item : irecipe.input)
			if (NEIClientUtils.areStacksSameTypeCrafting(item, ingredient))
				return true;
		return false;
	}

	@Override
	public void loadUsageRecipes(ItemStack ingredient)
	{
	    	for(AssemblyRecipe irecipe : AssemblyRecipe.assemblyRecipes)
		{
			//CachedAssemblyRecipe recipe = new CachedAssemblyRecipe(irecipe);
			//if(recipe.contains(recipe.ingredients,ingredient))
			if (recipeContain(irecipe, ingredient))
			{
				CachedAssemblyRecipe recipe = new CachedAssemblyRecipe(irecipe);
				recipe.setIngredientPermutation(recipe.ingredients,ingredient);
				arecipes.add(recipe);
			}
	    	}
	}

}
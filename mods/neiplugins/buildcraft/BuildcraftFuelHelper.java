package mods.neiplugins.buildcraft;

import java.util.HashMap;
import java.util.List;

import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraftforge.liquids.LiquidStack;

import buildcraft.api.fuels.IronEngineFuel;
import mods.neiplugins.common.FuelTooltip;
import mods.neiplugins.common.SimpleFuelContextHelper;
import mods.neiplugins.common.SimpleFuelHelper;

public class BuildcraftFuelHelper {
	public static Class guiCombustionEngine = null;
	public static Class guiSteamEngine = null;

	public static void registerFuelHelpers()
	{
		FuelTooltip.addFuelHelper(new SimpleFuelHelper("buildcraft.ironengine", "Combustion Engine", null) {
			@Override
			public List<String> getLiquidStackFuelTooltip(LiquidStack liquid, List<String> currenttip)
			{
				IronEngineFuel fuel = IronEngineFuel.getFuelForLiquid(liquid);
				if (fuel != null)
					currenttip.add(FuelTooltip.linePrefix+FuelTooltip.convertMJtF(fuel.powerPerCycle)+" MJ/t for "+fuel.totalBurningTime*liquid.amount/1000+" ticks ("+"Combustion Engine"+")");
				return currenttip;
			}
		});

		FuelTooltip.addFuelHelper(true, new SimpleFuelHelper("buildcraft.stoneengine", "Stirling Engine", null) {
			@Override
			public List<String> getItemStackFuelTooltip(ItemStack stack, List<String> currenttip)
			{
				int fuelValue = TileEntityFurnace.getItemBurnTime(stack);
				if (fuelValue > 0)
					currenttip.add(FuelTooltip.linePrefix+"1 MJ/t for "+fuelValue+" ticks ("+"Stirling Engine"+")");
				return currenttip;
			}
		});

		HashMap<Class, String> map = new HashMap<Class, String>();
		if (guiCombustionEngine != null)
			map.put(guiCombustionEngine, "buildcraft.ironengine");
		if (guiSteamEngine != null)
			map.put(guiSteamEngine, "buildcraft.stoneengine");
		if (!map.isEmpty())
			FuelTooltip.addContextFuelHelper(new SimpleFuelContextHelper("bc.engines","Buildcraft Engines",null,map));
	}


}
package mods.neiplugins.te;

import java.util.HashMap;
import java.util.List;

import net.minecraft.item.ItemStack;
import net.minecraftforge.liquids.LiquidStack;

import mods.neiplugins.common.FuelTooltip;
import mods.neiplugins.common.SimpleFuelContextHelper;
import mods.neiplugins.common.SimpleFuelHelper;
import mods.neiplugins.common.Utils;
import thermalexpansion.api.ThermalExpansionInfo;

public class TEFuelHelper {
	public static void registerFuelHelpers()
	{
		FuelTooltip.addFuelHelper(true, new SimpleFuelHelper("te.steamengine", "Steam Engine", null) {
			@Override
			public List<String> getItemStackFuelTooltip(ItemStack stack, List<String> currenttip)
			{
				int fuelValue = ThermalExpansionInfo.getFuelValue(stack);
				if (fuelValue > 0)
					currenttip.add(FuelTooltip.linePrefix+fuelValue+" MJ (Steam Engine)");
				return currenttip;
			}
		});

		FuelTooltip.addFuelHelper(new SimpleFuelHelper("te.magmaticengine", "Magmatic Engine", null) {
			@Override
			public List<String> getLiquidStackFuelTooltip(LiquidStack liquid, List<String> currenttip)
			{
				int fuelValue = ThermalExpansionInfo.getFuelValue(liquid);
				if (fuelValue>0) {
					fuelValue = (fuelValue * liquid.amount) / 1000;
					currenttip.add(FuelTooltip.linePrefix+fuelValue+" MJ (Magmatic Engine)");
				}
				return currenttip;
			}
		});

		HashMap<Class, String> map = new HashMap<Class, String>();

		Class cls = Utils.findClass("thermalexpansion.gui.gui.GuiEngineMagmatic");
		if (cls != null)
			map.put(cls, "te.magmaticengine");

		cls = Utils.findClass("thermalexpansion.gui.gui.GuiEngineSteam");
		if (cls != null)
			map.put(cls, "te.steamengine");

		if (!map.isEmpty())
			FuelTooltip.addContextFuelHelper(new SimpleFuelContextHelper("te.all","Thermal Expansion",null,map));


	}
}
package mods.neiplugins.common;



public interface IPlugin
{
	public String getPluginName();
	public String getPluginVersion();
	public boolean isValid();
	public void init();
	public void loadConfig();
}
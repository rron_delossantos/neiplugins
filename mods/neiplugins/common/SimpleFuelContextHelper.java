package mods.neiplugins.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.ItemStack;

import mods.neiplugins.common.FuelTooltip;
import mods.neiplugins.common.TooltipCache;

public class SimpleFuelContextHelper implements IFuelContextHelper
{
	protected String key;
	protected String name;
	protected List<String> desc;
	protected Map<Class, String> map;

	public SimpleFuelContextHelper(String key, String name, List<String> desc, Map<Class, String> map)
	{
		this.key = key;
		this.name = name;
		this.desc = desc;
		this.map = map;
		if (this.desc == null && !this.map.isEmpty()) {
			this.desc = new ArrayList<String>();
			this.desc.add("Registred tooltips:");
			for (String t : this.map.values()) {
				this.desc.add(FuelTooltip.registredHelpers.get(t).getName());
			}
		}
	}

	@Override
	public String getName()
	{
		return this.name;
	}

	@Override
	public String getKey()
	{
		return this.key;
	}

	@Override
	public List<String> getDescription()
	{
		return this.desc;
	}

	@Override
	public boolean displayFuelTooltip(GuiContainer gui)
	{
		return false;
	}

	@Override
	public boolean haveContextTooltip(GuiContainer gui) {
		return map.containsKey(gui.getClass());
	}

	@Override
	public List<String> getContextTooltip(GuiContainer gui, ItemStack stack, List<String> currenttip)
	{
		TooltipCache cache = FuelTooltip.getContextCache(gui);
		if (cache.addCachedTooltip(stack, currenttip))
			return currenttip;
		String hKey = map.get(gui.getClass());
		if (hKey != null)
		{
			FuelTooltip.addContextTooltipCached(gui, currenttip, stack, hKey);
		}
		return currenttip;
	}


}
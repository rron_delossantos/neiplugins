package mods.neiplugins.common;

import java.util.List;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.ItemStack;

public interface IFuelContextHelper {
	public String getName();
	public String getKey();
	public List<String> getDescription();
	public boolean displayFuelTooltip(GuiContainer gui);
	public boolean haveContextTooltip(GuiContainer gui);
	public List<String> getContextTooltip(GuiContainer gui, ItemStack stack, List<String> currenttip);
}
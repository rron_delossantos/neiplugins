package mods.neiplugins.common;

import java.util.ArrayList;

import mods.neiplugins.lists.IListElement;

public abstract class Registry {
	public static ArrayList<IListElement> infoListMenu = new ArrayList<IListElement>();
	public static ArrayList<IListElement> settingsListMenu = new ArrayList<IListElement>();
	public static ArrayList<IListElement> developerListMenu = new ArrayList<IListElement>();
}
package mods.neiplugins.common;

import java.util.ArrayList;

public interface ICachedRecipeWithLiquidTank
{
	public ArrayList<LiquidTank> getLiquidTanks();
}
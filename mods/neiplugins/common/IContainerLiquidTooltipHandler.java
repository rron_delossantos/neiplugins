package mods.neiplugins.common;

import java.util.List;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraftforge.liquids.LiquidStack;

import codechicken.nei.forge.IContainerTooltipHandler;

public interface IContainerLiquidTooltipHandler extends IContainerTooltipHandler
{
    public List<String> handleLiquidTooltip(GuiContainer gui, LiquidStack liquid, List<String> currenttip);
}
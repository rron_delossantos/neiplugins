package mods.neiplugins.common;

import java.util.EnumSet;

import cpw.mods.fml.common.IScheduledTickHandler;
import cpw.mods.fml.common.TickType;
import cpw.mods.fml.common.event.FMLInterModComms;
import cpw.mods.fml.common.event.FMLInterModComms.IMCMessage;

import mods.neiplugins.NEIPlugins;

public class TickHandlerIMCMessages implements IScheduledTickHandler
{
	private int nextSpacing = 20;

	public void reset()
	{
		nextSpacing = 20;
	}

	@Override
	public void tickStart(EnumSet type, Object[] tickData)
	{
		nextSpacing += 20;
		if (nextSpacing > 400)
			nextSpacing = 400;
		for (IMCMessage message : FMLInterModComms.fetchRuntimeMessages(NEIPlugins.getMod()))
			IMCHandler.processIMCMessage(message);
	}

	@Override
	public void tickEnd(EnumSet type, Object[] tickData)
	{
	}

	@Override
	public EnumSet ticks()
	{
		return EnumSet.of(TickType.SERVER);
	}

	@Override
	public String getLabel()
	{
		return "NEIPlugins IMC Messages";
	}

	@Override
	public int nextTickSpacing()
	{
		return nextSpacing;
	}
}
package mods.neiplugins.common;

import java.util.Comparator;

import net.minecraft.item.ItemStack;

public class ItemStackComparator implements Comparator<ItemStack>
{
	public ItemStackComparator() {}
        public int compare(ItemStack o1, ItemStack o2) {
		if (o1.itemID != o2.itemID) return Integer.valueOf(o1.itemID).compareTo((int) o2.itemID);
		if (o1.getItemDamage() != o2.getItemDamage()) return Integer.valueOf(o1.getItemDamage()).compareTo((int) o2.getItemDamage());
		return 0;
	}
}
package mods.neiplugins.common;

import java.util.List;

import codechicken.nei.PositionedStack;
import codechicken.nei.recipe.GuiRecipe;

public class PositionedStackWithTooltip extends PositionedStack implements IPositionedStackWithTooltipHandler
{
	public String tooltip;

	public PositionedStackWithTooltip(Object object, int x, int y)
	{
		super(object, x, y);
	}

	@Override
	public List<String> handleTooltip(GuiRecipe guiRecipe, List<String> currenttip)
	{
		if (this.tooltip != null)
			currenttip.add(this.tooltip);
		return currenttip;
	}

	public void setTooltip(String tooltip)
	{
		this.tooltip = tooltip;
	}

	public void setIntegerChanceTooltip(int chance)
	{
		if (chance < 1)
			this.tooltip = "Chance: never";
		else if (chance != 100)
			this.tooltip = "Chance: "+Integer.toString(chance)+"%";
		else
			this.tooltip = null;
	}
}
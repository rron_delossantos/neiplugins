package mods.neiplugins.common;

import java.util.logging.Level;
import java.util.logging.Level;

import cpw.mods.fml.common.event.FMLInterModComms.IMCMessage;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;

import mods.neiplugins.NEIPlugins;
import mods.neiplugins.common.IMCHandler;

public abstract class IMCHandler {
	private static TickHandlerIMCMessages handleIMCMessages;

	public static void postInit()
	{
		handleIMCMessages = new TickHandlerIMCMessages();
		handleIMCMessages.reset();
		TickRegistry.registerScheduledTickHandler(handleIMCMessages, Side.SERVER);
	}
	public static void processIMCMessage(IMCMessage message)
	{
		NEIPlugins.logFine("Accepted IMC message {0} with type {1}",message.key,message.getMessageType().getName());
		if (message.key.equals("register-usage-handler")) {
			String[] tokens = message.getStringValue().split("@");
			if (tokens.length != 3) {
				NEIPlugins.log(Level.INFO, String.format("Received an invalid 'register-usage-handler' request %s from mod %s", message.getStringValue(), message.getSender()));
				return;
			}
			if (!RecipeHandlerUtils.addToRecipeList(tokens[0],tokens[1],1,tokens[2])) {
				NEIPlugins.log(Level.INFO, String.format("Received 'register-usage-handler' request %s from mod %s for recipe ID that already registred", message.getStringValue(), message.getSender()));
				return;
			}
		}
		else if (message.key.equals("register-crafting-handler")) {
			String[] tokens = message.getStringValue().split("@");
			if (tokens.length != 3) {
				NEIPlugins.log(Level.INFO, String.format("Received an invalid 'register-crafting-handler' request %s from mod %s", message.getStringValue(), message.getSender()));
				return;
			}
			if (!RecipeHandlerUtils.addToRecipeList(tokens[0],tokens[1],0,tokens[2])) {
				NEIPlugins.log(Level.INFO, String.format("Received 'register-crafting-handler' request %s from mod %s for recipe ID that already registred", message.getStringValue(), message.getSender()));
				return;
			}
		}
	}
}
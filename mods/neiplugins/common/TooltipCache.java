package mods.neiplugins.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

public class TooltipCache {
	private Map<Integer, List<String>> cachedTooltips;
	private boolean ignoreNBT;

	public TooltipCache(boolean ignoreNBT)
	{
		this.cachedTooltips = new HashMap<Integer, List<String>>();
		this.ignoreNBT = ignoreNBT;
	}

	public TooltipCache()
	{
		this(false);
	}

	protected int stackToInt(ItemStack aStack) {
		if (aStack == null) return 0;
		return aStack.itemID | (aStack.getItemDamage()<<16);
	}

	protected int stackWildcardToInt(ItemStack aStack) {
		if (aStack == null) return 0;
		return aStack.itemID | (OreDictionary.WILDCARD_VALUE << 16);
	}
	public boolean isCached(ItemStack stack)
	{
		return (ignoreNBT || !stack.hasTagCompound()) && cachedTooltips.containsKey(stackToInt(stack));
	}

	public boolean addCachedWildcardTooltip(ItemStack stack, List<String> currenttip)
	{
		boolean result = addCachedTooltip(stack, currenttip);
		if (result || stack.hasTagCompound() && !ignoreNBT)
			return result;

		Integer key = stackWildcardToInt(stack);
		if (cachedTooltips.containsKey(key))
		{
			List<String> t = cachedTooltips.get(key);
			if (t != null)
				currenttip.addAll(t);
			return true;
		}
		return result;
	}

	public boolean addCachedTooltip(ItemStack stack, List<String> currenttip)
	{
		if (stack.hasTagCompound() && !ignoreNBT)
			return false;
		Integer key = stackToInt(stack);
		if (cachedTooltips.containsKey(key))
		{
			List<String> t = cachedTooltips.get(key);
			if (t != null)
				currenttip.addAll(t);
			return true;
		}
		return false;
	}

	public void put(ItemStack stack, List<String> currenttip)
	{
		Integer key = stackToInt(stack);
		cachedTooltips.put(key, currenttip);
	}

	public void clear()
	{
		cachedTooltips.clear();
	}
}
package mods.neiplugins.common;

import java.awt.Point;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.List;
import java.util.List;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Container;
import net.minecraft.item.ItemStack;
import net.minecraftforge.liquids.LiquidStack;

import codechicken.nei.NEIClientConfig;
import codechicken.nei.PositionedStack;
import codechicken.nei.forge.GuiContainerManager;
import codechicken.nei.forge.GuiContainerManager;
import codechicken.nei.recipe.GuiCraftingRecipe;
import codechicken.nei.recipe.GuiRecipe;
import codechicken.nei.recipe.GuiRecipe;
import codechicken.nei.recipe.GuiUsageRecipe;
import codechicken.nei.recipe.TemplateRecipeHandler;
import codechicken.nei.recipe.TemplateRecipeHandler;

import mods.neiplugins.NEIPlugins;
import mods.neiplugins.common.ICachedRecipeWithLiquidTank;
import mods.neiplugins.common.IPositionedStackWithTooltipHandler;
import mods.neiplugins.common.LiquidTank;

public abstract class LiquidTemplateRecipeHandler extends TemplateRecipeHandler
{
	public abstract String getRecipeId();

	@Override
	public boolean hasOverlay(GuiContainer gui, Container container, int recipe)
	{
		return false;
	}

	public void loadCraftingRecipes(LiquidStack result)
	{
	}

	public void loadUsageRecipes(LiquidStack ingredient)
	{
	}

	public void loadSameRecipeId()
	{
	}

	@Override
	public void loadCraftingRecipes(String outputId, Object... results)
	{
		try {
			if(outputId.equals(getRecipeId()))
				loadSameRecipeId();
			else if (outputId.equals("liquid") && results.length == 1 && results[0] instanceof LiquidStack)
				loadCraftingRecipes((LiquidStack)results[0]);
			else
				super.loadCraftingRecipes(outputId, results);
		} catch (Exception ex) {
			NEIPlugins.logWarning("Error occured while calling LiquidTemplateRecipeHandler.loadCraftingRecipes for recipeId: {0}", ex, getRecipeId());
		}
	}


	@Override
	public void loadUsageRecipes(String inputId, Object... ingredients)
	{
		try {
			if (inputId.equals("liquid") && ingredients.length == 1 && ingredients[0] instanceof LiquidStack)
				loadUsageRecipes((LiquidStack)ingredients[0]);
			else
				super.loadUsageRecipes(inputId, ingredients);
		} catch (Exception ex) {
			NEIPlugins.logWarning("Error occured while calling LiquidTemplateRecipeHandler.loadUsageRecipes for recipeId: {0}", ex, getRecipeId());
		}
	}


	@Override
	public void drawForeground(GuiContainerManager gui, int recipe)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glDisable(GL11.GL_LIGHTING);
		gui.bindTexture(getGuiTexture());
	        drawExtras(gui, recipe);
		drawLiquidTanks(gui, recipe);
	}

	public void drawLiquidTanks(GuiContainerManager gui, int recipe)
	{
		CachedRecipe crecipe = arecipes.get(recipe);
		if (crecipe instanceof ICachedRecipeWithLiquidTank)
		{
			for (LiquidTank tank : ((ICachedRecipeWithLiquidTank)crecipe).getLiquidTanks())
			{
				tank.draw(gui, getGuiTexture());
			}
		}
	}

	protected boolean transferLiquidTank(GuiRecipe guiRecipe, int recipe, boolean usage)
	{
		CachedRecipe crecipe = arecipes.get(recipe);
		if (crecipe instanceof ICachedRecipeWithLiquidTank)
		{
			Point mousepos = guiRecipe.manager.getMousePosition();
			Point offset = guiRecipe.getRecipePosition(recipe);
			Point relMouse = new Point(mousepos.x - guiRecipe.guiLeft - offset.x, mousepos.y - guiRecipe.guiTop - offset.y);

			for (LiquidTank tank : ((ICachedRecipeWithLiquidTank)crecipe).getLiquidTanks())
			{
				if (tank.position.contains(relMouse))
				{
					if (tank.liquid.itemID > 0 && (usage ?
							GuiUsageRecipe.openRecipeGui("liquid", tank.liquid) :
							GuiCraftingRecipe.openRecipeGui("liquid", tank.liquid)))
						return true;
				}
			}
		}
		return false;
	}

	@Override
	public List<String> handleTooltip(GuiRecipe guiRecipe, List<String> currenttip, int recipe)
	{
		currenttip = super.handleTooltip(guiRecipe, currenttip, recipe);
		CachedRecipe crecipe = arecipes.get(recipe);
		if (guiRecipe.manager.shouldShowTooltip() && crecipe instanceof ICachedRecipeWithLiquidTank)
		{
			Point mousepos = guiRecipe.manager.getMousePosition();
			Point offset = guiRecipe.getRecipePosition(recipe);
			Point relMouse = new Point(mousepos.x - guiRecipe.guiLeft - offset.x, mousepos.y - guiRecipe.guiTop - offset.y);

			for (LiquidTank tank : ((ICachedRecipeWithLiquidTank)crecipe).getLiquidTanks())
			{
				if (tank.position.contains(relMouse))
				{
					tank.handleTooltip(currenttip);
				}
			}
		}
		return currenttip;
	}

	public Rectangle getRectangleFromPositionedStack(PositionedStack stack)
	{
		return new Rectangle(stack.relx-1, stack.rely-1, 18, 18);
	}

	@Override
	public List<String> handleItemTooltip(GuiRecipe gui, ItemStack itemStack, List<String> currenttip, int recipe)
	{
		CachedRecipe irecipe = arecipes.get(recipe);
		Point mousepos = gui.manager.getMousePosition();
		Point offset = gui.getRecipePosition(recipe);
		Point relMouse = new Point(mousepos.x - gui.guiLeft - offset.x, mousepos.y - gui.guiTop - offset.y);

		for(PositionedStack stack : irecipe.getIngredients())
			if (stack instanceof IPositionedStackWithTooltipHandler && getRectangleFromPositionedStack(stack).contains(relMouse))
				currenttip = ((IPositionedStackWithTooltipHandler)stack).handleTooltip(gui, currenttip);

		for(PositionedStack stack : irecipe.getOtherStacks())
			if (stack instanceof IPositionedStackWithTooltipHandler && getRectangleFromPositionedStack(stack).contains(relMouse))
				currenttip = ((IPositionedStackWithTooltipHandler)stack).handleTooltip(gui, currenttip);

		PositionedStack stack = irecipe.getResult();
		if (stack instanceof IPositionedStackWithTooltipHandler && getRectangleFromPositionedStack(stack).contains(relMouse))
			currenttip = ((IPositionedStackWithTooltipHandler)stack).handleTooltip(gui, currenttip);

		return currenttip;
	}

	@Override
	public boolean keyTyped(GuiRecipe gui, char keyChar, int keyCode, int recipe)
	{
		if(keyCode == NEIClientConfig.getKeyBinding("recipe"))
		{
			if (transferLiquidTank(gui, recipe, false))
				return true;
		}
		else if(keyCode == NEIClientConfig.getKeyBinding("usage"))
		{
			if (transferLiquidTank(gui, recipe, true))
				return true;
		}
		return super.keyTyped(gui, keyChar, keyCode, recipe);
	}

	@Override
	public boolean mouseClicked(GuiRecipe gui, int button, int recipe)
	{
		if(button == 0)
		{
			if (transferLiquidTank(gui, recipe, false))
				return true;
		}
		else if(button == 1)
		{
			if (transferLiquidTank(gui, recipe, true))
				return true;
		}
		return super.mouseClicked(gui, button, recipe);
	}
}
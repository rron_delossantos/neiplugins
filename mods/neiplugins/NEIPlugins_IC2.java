package mods.neiplugins;

import java.awt.Rectangle;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Map;

import net.minecraft.block.Block;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import cpw.mods.fml.common.event.FMLInterModComms;

import codechicken.nei.MultiItemRange;
import codechicken.nei.api.API;
import codechicken.nei.recipe.TemplateRecipeHandler;

import ic2.api.item.Items;
import mods.neiplugins.common.FuelTooltip;
import mods.neiplugins.common.IPlugin;
import mods.neiplugins.common.ItemApiHelper;
import mods.neiplugins.common.RecipeHandlerUtils;
import mods.neiplugins.common.Utils;
import mods.neiplugins.ic2.GuiValuableOreList;
import mods.neiplugins.ic2.IC2FuelHelper;
import mods.neiplugins.ic2.MassFabricatorRecipeHandler;
import mods.neiplugins.options.OptionsIC2;

public class NEIPlugins_IC2 implements IPlugin {

	public static final String PLUGIN_NAME = "IC2";
	public static final String PLUGIN_VERSION = "1.1.0";
	public static final String REQUIRED_MOD = "IC2@[1.115.320-lf,)";

	@Override
	public String getPluginName() {
		return PLUGIN_NAME;
	}

	@Override
	public String getPluginVersion() {
		return PLUGIN_VERSION;
	}

	@Override
	public boolean isValid() {
		return NEIPlugins.getMod().hasMod(REQUIRED_MOD);
	}

	@Override
	public void init() {
	}

	@Override
	public void loadConfig() {
		configureItemPanel();
		initHandlers();
		addTransferRects();
		OptionsIC2.addOptions();
	}

	public static ItemStack getIc2Item(String name)
	{
		return Items.getItem(name);
	}

	private void addTransferRects() {
		Class cls = (Class<? extends GuiContainer>)Utils.findClass("ic2.core.block.machine.gui.GuiIronFurnace");
		ArrayList<Class<? extends GuiContainer>> guis = new ArrayList<Class<? extends GuiContainer>>();
		ArrayList<TemplateRecipeHandler.RecipeTransferRect> transferRects = new ArrayList<TemplateRecipeHandler.RecipeTransferRect>();

		if (cls != null) {
			guis.add(cls);
			transferRects.add(new TemplateRecipeHandler.RecipeTransferRect(new Rectangle(50, 23, 18, 18), "fuel"));
			transferRects.add(new TemplateRecipeHandler.RecipeTransferRect(new Rectangle(74, 23, 24, 18), "smelting"));
			TemplateRecipeHandler.RecipeTransferRectHandler.registerRectsToGuis(guis, transferRects);
		}

		cls = (Class<? extends GuiContainer>)Utils.findClass("ic2.core.block.machine.gui.GuiElecFurnace");
		guis = new ArrayList<Class<? extends GuiContainer>>();
		transferRects = new ArrayList<TemplateRecipeHandler.RecipeTransferRect>();
		if (cls != null) {
			guis.add(cls);
			transferRects.add(new TemplateRecipeHandler.RecipeTransferRect(new Rectangle(74, 23, 24, 18), "smelting"));
			TemplateRecipeHandler.RecipeTransferRectHandler.registerRectsToGuis(guis, transferRects);
		}


		/*cls = (Class<? extends GuiContainer>)Utils.findClass("ic2.core.block.machine.gui.GuiInduction");
		guis = new ArrayList<Class<? extends GuiContainer>>();
		transferRects = new ArrayList<TemplateRecipeHandler.RecipeTransferRect>();

		if (cls != null) {
			guis.add(cls);
			transferRects.add(new TemplateRecipeHandler.RecipeTransferRect(new Rectangle(74, 23, 24, 18), "smelting"));
			TemplateRecipeHandler.RecipeTransferRectHandler.registerRectsToGuis(guis, transferRects);
		}*/
	}

	private void initHandlers() {
		MassFabricatorRecipeHandler.guiclass = (Class<? extends GuiContainer>)Utils.findClass("ic2.core.block.machine.gui.GuiMatter");

		Class cls = Utils.findClass("ic2.core.IC2");
		if (cls != null)
			GuiValuableOreList.valuableOres = (Map<Integer,Map<Integer,Integer>>)Utils.getField(cls, "valuableOres", null);

		//FMLInterModComms.sendRuntimeMessage(NEIPlugins.getMod(),"NEIPlugins","register-crafting-handler","IC2@Mass Fabricator@ic2.matter");
		//FMLInterModComms.sendRuntimeMessage(NEIPlugins.getMod(),"NEIPlugins","register-crafting-handler","IC2@Compressor@ic2.compressor");
		//FMLInterModComms.sendRuntimeMessage(NEIPlugins.getMod(),"NEIPlugins","register-crafting-handler","IC2@Compressor2@ic2.compressor");
		RecipeHandlerUtils.addToRecipeList("IC2",getIc2Item("compressor"),"Compressor",0,"ic2.compressor");
		RecipeHandlerUtils.addToRecipeList("IC2",getIc2Item("extractor"),"Extractor",0,"ic2.extractor");
		RecipeHandlerUtils.addToRecipeList("IC2",getIc2Item("macerator"),"Macerator",0,"ic2.macerator");
		if (getIc2Item("matter") != null) {
			MassFabricatorRecipeHandler.addEmptyScrap = true;
			API.registerRecipeHandler(new MassFabricatorRecipeHandler());
			API.registerUsageHandler(new MassFabricatorRecipeHandler());
			RecipeHandlerUtils.addToRecipeList("IC2",getIc2Item("massFabricator"),"Mass Fabricator",0,"ic2.matter");
		}
		RecipeHandlerUtils.addToRecipeList("IC2",getIc2Item("scrapBox"),"Scrapbox",0,"ic2.scrapbox");

		IC2FuelHelper.registerFuelHelpers();
		FuelTooltip.addValidNBTItem(getIc2Item("filledFuelCan"));
	}

	private void configureItemPanel() {
		MultiItemRange electricItems = new MultiItemRange();
		MultiItemRange reactorComponents = new MultiItemRange();
		MultiItemRange tfbp = new MultiItemRange();
		MultiItemRange painters = new MultiItemRange();

		ItemApiHelper.hideItem(getIc2Item("reinforcedDoorBlock"));
		ItemApiHelper.hideItem(getIc2Item("activeLuminator"));
		ItemApiHelper.hideItem(getIc2Item("miningPipeTip"));
		ItemApiHelper.hideItem(getIc2Item("dynamiteStick"));
		ItemApiHelper.hideItem(getIc2Item("dynamiteStickWithRemote"));
		ItemApiHelper.hideItem(getIc2Item("constructionFoamWall"));
		ItemApiHelper.hideItem(getIc2Item("blockBarrel"));
		ItemApiHelper.hideItem(getIc2Item("enabledNanoSaber"));
		ItemApiHelper.hideItem(getIc2Item("copperCableBlock"));

		Class IElectricItem = Utils.findClass("ic2.api.item.IElectricItem");
		Class IReactorComponent = Utils.findClass("ic2.api.reactor.IReactorComponent");
		Class ITerraformingBP = Utils.findClass("ic2.api.item.ITerraformingBP");
		for (Item i:Item.itemsList)
		{
			if (IElectricItem != null && IElectricItem.isInstance(i))
				electricItems.add(i.itemID);
			if (IReactorComponent != null && IReactorComponent.isInstance(i))
				ItemApiHelper.addItemToRangeWithNBT(reactorComponents, i);
			if (ITerraformingBP != null && ITerraformingBP.isInstance(i))
				tfbp.add(i.itemID);
		}

		ItemApiHelper.addItemStackToRange(painters, getIc2Item("blackPainter"));
		ItemApiHelper.addItemStackToRange(painters, getIc2Item("redPainter"));
		ItemApiHelper.addItemStackToRange(painters, getIc2Item("greenPainter"));
		ItemApiHelper.addItemStackToRange(painters, getIc2Item("brownPainter"));
		ItemApiHelper.addItemStackToRange(painters, getIc2Item("bluePainter"));
		ItemApiHelper.addItemStackToRange(painters, getIc2Item("purplePainter"));
		ItemApiHelper.addItemStackToRange(painters, getIc2Item("cyanPainter"));
		ItemApiHelper.addItemStackToRange(painters, getIc2Item("lightGreyPainter"));
		ItemApiHelper.addItemStackToRange(painters, getIc2Item("darkGreyPainter"));
		ItemApiHelper.addItemStackToRange(painters, getIc2Item("pinkPainter"));
		ItemApiHelper.addItemStackToRange(painters, getIc2Item("limePainter"));
		ItemApiHelper.addItemStackToRange(painters, getIc2Item("yellowPainter"));
		ItemApiHelper.addItemStackToRange(painters, getIc2Item("cloudPainter"));
		ItemApiHelper.addItemStackToRange(painters, getIc2Item("magentaPainter"));
		ItemApiHelper.addItemStackToRange(painters, getIc2Item("orangePainter"));
		ItemApiHelper.addItemStackToRange(painters, getIc2Item("whitePainter"));

		API.addSetRange("IC2 Related.Electric Items", electricItems);
		API.addSetRange("IC2 Related.Reactor Components", reactorComponents);
		API.addSetRange("IC2 Related.Terraformer Blueprints", tfbp);
		API.addSetRange("IC2 Related.Tools.Painters", painters);

		ItemApiHelper.addSetRangeFromItem("IC2 Related.Seeds", getIc2Item("cropSeed"));

	}


}
package mods.neiplugins;

import java.util.logging.Level;

import net.minecraft.item.ItemStack;

import codechicken.nei.MultiItemRange;
import codechicken.nei.api.API;
import codechicken.nei.recipe.DefaultOverlayHandler;

import mods.neiplugins.buildcraft.AssemblyRecipeHandler;
import mods.neiplugins.buildcraft.BuildcraftFuelHelper;
import mods.neiplugins.buildcraft.IronEngineFuelsHandler;
import mods.neiplugins.buildcraft.RefineryRecipeHandler;
import mods.neiplugins.common.IPlugin;
import mods.neiplugins.common.ItemApiHelper;
import mods.neiplugins.common.RecipeHandlerUtils;
import mods.neiplugins.common.Utils;

public class NEIPlugins_Buildcraft implements IPlugin {

	public static final String PLUGIN_NAME = "BuildCraft";
	public static final String PLUGIN_VERSION = "1.5.0";
	public static final String REQUIRED_MOD = "BuildCraft|Core";

	public static final String REQUIRED_MOD1 = "BuildCraft|Builders";
	public static final String REQUIRED_MOD2 = "BuildCraft|Energy";
	public static final String REQUIRED_MOD3 = "BuildCraft|Factory";
	public static final String REQUIRED_MOD4 = "BuildCraft|Silicon";
	public static final String REQUIRED_MOD5 = "BuildCraft|Transport";

	public static boolean buildersLoaded;
	public static boolean energyLoaded;
	public static boolean factoryLoaded;
	public static boolean siliconLoaded;
	public static boolean transportLoaded;

	private static ItemStack assemblyTable = null;
	private static ItemStack refineryBlock = null;

	@Override
	public String getPluginName() {
		return PLUGIN_NAME;
	}

	@Override
	public String getPluginVersion() {
		return PLUGIN_VERSION;
	}

	@Override
	public boolean isValid() {
		return NEIPlugins.getMod().hasMod(REQUIRED_MOD);
	}

	@Override
	public void init() {
	}


	@Override
	public void loadConfig() {
		buildersLoaded = NEIPlugins.getMod().hasMod(REQUIRED_MOD1);
		energyLoaded = NEIPlugins.getMod().hasMod(REQUIRED_MOD2);
		factoryLoaded = NEIPlugins.getMod().hasMod(REQUIRED_MOD3);
		siliconLoaded = NEIPlugins.getMod().hasMod(REQUIRED_MOD4);
		transportLoaded = NEIPlugins.getMod().hasMod(REQUIRED_MOD5);

		configureItemPanel();
		addHandlers();
	}

	private void addHandlers() {
		if (energyLoaded) {
			API.registerRecipeHandler(new IronEngineFuelsHandler());
			API.registerUsageHandler(new IronEngineFuelsHandler());

			BuildcraftFuelHelper.guiCombustionEngine = Utils.findClass("buildcraft.energy.gui.GuiCombustionEngine");
			BuildcraftFuelHelper.guiSteamEngine = Utils.findClass("buildcraft.energy.gui.GuiSteamEngine");
		} else {
			NEIPlugins.log(Level.INFO, "Mod Buildcraft|Energy not found", new Object[0]);
		}	
		if (buildersLoaded) {
			API.registerRecipeHandler(new RefineryRecipeHandler());
			API.registerUsageHandler(new RefineryRecipeHandler());
			RecipeHandlerUtils.addToRecipeList("Buildcraft", refineryBlock, "Refinery" , 0, "buildcraft.refinery");
		}

		if (siliconLoaded) {
			API.registerRecipeHandler(new AssemblyRecipeHandler());
			API.registerUsageHandler(new AssemblyRecipeHandler());
			RecipeHandlerUtils.addToRecipeList("Buildcraft",assemblyTable,"Assembly Table",0,"buildcraft.assembly");
			Class cls = Utils.findClass("buildcraft.silicon.gui.GuiAssemblyTable");
			if (cls != null) {
				API.registerGuiOverlay(cls, "buildcraft.assemblyTable", -17, 30);
				API.registerGuiOverlayHandler(cls, new DefaultOverlayHandler(-17, 30), "buildcraft.assemblyTable");
			}

		} else {
			NEIPlugins.log(Level.INFO, "Mod Buildcraft|Silicon not found", new Object[0]);
		}
		BuildcraftFuelHelper.registerFuelHelpers();
	}

	private void configureItemPanel() {
		Class cls = Utils.findClass("buildcraft.BuildCraftCore");
		if (cls == null)
			return;


		MultiItemRange blocks = new MultiItemRange();
		MultiItemRange blocksFactory = new MultiItemRange();
		MultiItemRange blocksBuilders = new MultiItemRange();

		MultiItemRange parts = new MultiItemRange();
		MultiItemRange transportPipes = new MultiItemRange();
		MultiItemRange liquidPipes = new MultiItemRange();
		MultiItemRange powerPipes = new MultiItemRange();
		MultiItemRange pipes = new MultiItemRange();

		MultiItemRange liquids = new MultiItemRange();

		MultiItemRange silicon = new MultiItemRange();

		String[] fields = new String[] {"woodenGearItem", "stoneGearItem" ,"ironGearItem" ,"goldGearItem" ,"diamondGearItem" ,"wrenchItem"};
		for (String field : fields)
			ItemApiHelper.addByFieldNameToRange(parts, cls, field);

		ItemApiHelper.addByFieldNameToRange(blocks, cls, "springBlock");
		ItemApiHelper.setOverrideNameByFieldName("Water Spring", cls, "springBlock");

		if (NEIPlugins_Buildcraft.buildersLoaded) {
			cls = Utils.findClass("buildcraft.BuildCraftBuilders");
			if (cls != null) {
				fields = new String[] {"markerBlock", "pathMarkerBlock" ,"fillerBlock" ,"builderBlock" ,"architectBlock" ,"libraryBlock"};
				for (String field : fields)
					ItemApiHelper.addByFieldNameToRange(blocksBuilders, cls, field);

				ItemApiHelper.addByFieldNameToRange(parts, cls, "templateItem");
				ItemApiHelper.addByFieldNameToRange(parts, cls, "blueprintItem");
			}
		}
		if (NEIPlugins_Buildcraft.energyLoaded) {
			cls = Utils.findClass("buildcraft.BuildCraftEnergy");
			if (cls != null) {
				fields = new String[] {"oilMoving", "oilStill" ,"bucketOil" ,"bucketFuel" ,"fuel"};
				for (String field : fields)
					ItemApiHelper.addByFieldNameToRange(liquids, cls, field);

				ItemApiHelper.addByFieldNameToRange(blocks, cls, "engineBlock");
			}
		}
		if (NEIPlugins_Buildcraft.factoryLoaded) {
			cls = Utils.findClass("buildcraft.BuildCraftFactory");
			if (cls != null) {
				fields = new String[] {"quarryBlock", "miningWellBlock" ,"autoWorkbenchBlock" ,"pumpBlock" ,"tankBlock", "refineryBlock" ,"hopperBlock"};
				for (String field : fields)
					ItemApiHelper.addByFieldNameToRange(blocksFactory, cls, field);

				ItemApiHelper.hideItemByFieldName(cls, "frameBlock");
				ItemApiHelper.hideItemByFieldName(cls, "plainPipeBlock");
				refineryBlock = ItemApiHelper.getItemStackByFieldName(cls, null, "refineryBlock");
			}

		}
		if (NEIPlugins_Buildcraft.siliconLoaded) {
			cls = Utils.findClass("buildcraft.BuildCraftSilicon");
			if (cls != null) {
				ItemApiHelper.addByFieldNameToRange(blocks, cls, "laserBlock");
				ItemApiHelper.addByFieldNameToRange(blocks, cls, "assemblyTableBlock");
				ItemApiHelper.addByFieldNameToRange(silicon, cls, "redstoneChipset");
				assemblyTable = ItemApiHelper.getItemStackByFieldName(cls, null, "assemblyTableBlock");
			}
		}
		if (NEIPlugins_Buildcraft.transportLoaded) {
			cls = Utils.findClass("buildcraft.BuildCraftTransport");
			if (cls != null) {
				fields = new String[] {"redPipeWire", "bluePipeWire", "greenPipeWire", "yellowPipeWire"};
				for (String field : fields)
					ItemApiHelper.addByFieldNameToRange(silicon, cls, field);

				fields = new String[] {"pipeItemsWood", "pipeItemsStone", "pipeItemsCobblestone", "pipeItemsIron", "pipeItemsGold", "pipeItemsDiamond", "pipeItemsObsidian", "pipeItemsVoid", "pipeItemsSandstone", "pipeItemsEmerald"};
				for (String field : fields)
					ItemApiHelper.addByFieldNameToRange(transportPipes, cls, field);

				fields = new String[] {"pipeLiquidsWood", "pipeLiquidsCobblestone", "pipeLiquidsStone", "pipeLiquidsIron", "pipeLiquidsGold", "pipeLiquidsVoid", "pipeLiquidsSandstone", "pipeLiquidsEmerald"};
				for (String field : fields)
					ItemApiHelper.addByFieldNameToRange(liquidPipes, cls, field);

				ItemApiHelper.hideItemByFieldName(cls, "genericPipeBlock");
				ItemApiHelper.addByFieldNameToRange(powerPipes, cls, "pipePowerWood");
				ItemApiHelper.addByFieldNameToRange(powerPipes, cls, "pipePowerStone");
				ItemApiHelper.addByFieldNameToRange(powerPipes, cls, "pipePowerGold");
				ItemApiHelper.addByFieldNameToRange(pipes, cls, "pipeStructureCobblestone");
				ItemApiHelper.addByFieldNameToRange(pipes, cls, "plugItem");
				ItemApiHelper.addByFieldNameToRange(parts, cls, "pipeWaterproof");
				ItemApiHelper.addSetRangeFromItem("Buildcraft.Silicon.Gates", cls, "pipeGate");
				ItemApiHelper.addSetRangeFromItem("Buildcraft.Silicon.Gates.Autarchic", cls, "pipeGateAutarchic");
				ItemApiHelper.addSetRangeFromItem("Buildcraft.Facades", cls, "facadeItem");
			}
		}
		API.addSetRange("Buildcraft.Blocks", blocks);
		API.addSetRange("Buildcraft.Blocks.Builders", blocksBuilders);
		API.addSetRange("Buildcraft.Blocks.Factory", blocksFactory);

		API.addSetRange("Buildcraft.Items", parts);
		API.addSetRange("Buildcraft.Liquids", liquids);

		API.addSetRange("Buildcraft.Pipes", pipes);
		API.addSetRange("Buildcraft.Pipes.Transport", transportPipes);
		API.addSetRange("Buildcraft.Pipes.Liquid", liquidPipes);
		API.addSetRange("Buildcraft.Pipes.Power", powerPipes);

		API.addSetRange("Buildcraft.Silicon", silicon);

	}
}
package mods.neiplugins.lists;

import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import codechicken.nei.PositionedStack;
import codechicken.nei.forge.GuiContainerManager;

import mods.neiplugins.common.Utils;

public class MultipleStackListElement implements IListElement
{
	public MultipleStackListElement(Iterator<ItemStack> stacks, int margin, Dimension size)
	{
		int pos = 0;
		this.margin = margin;
		this.stacks = new ArrayList<PositionedStack>();

		ItemStack stack;
		while ((pos + 16 < size.width) && stacks.hasNext()) {
			stack = stacks.next();
			if (stack == null || !Utils.isSafeItemStack(stack, "mistaqur.nei.lists.MultipleStackListElement"))
				continue;
			this.stacks.add(new PositionedStack(stack,pos,1));
			pos += 16 + margin;
		}
	}
	protected MultipleStackListElement()
	{
	}

	public void draw(GuiContainerManager gui, Dimension size)
	{
		//gui.drawText(18, 1, this.title);
	}
	public boolean click(int button)
	{
		return false;
	}
	public List<String> handleTooltip(GuiList gui, List<String> currenttip, Point mousepos)
	{
		return currenttip;
	}

	protected List<String> handleItemTooltip(GuiList gui, int index, List<String> currenttip, Point mousePos)
	{
		return currenttip;
	}

	public List<String> handleItemTooltip(GuiList gui, ItemStack stack, List<String> currenttip, Point mousePos)
	{
		if (mousePos.y<0 || mousePos.y>17)
			return currenttip;
		int t = 16+this.margin;
		int index = mousePos.x/t;
		if (index >= 0 && index < stacks.size() && (mousePos.x%t)<18) {
			handleItemTooltip(gui, index, currenttip, mousePos);
		}
		return currenttip;
	}

	public ArrayList<PositionedStack> getStacks() {
		return stacks;
	}


	protected int margin;
	public ArrayList<PositionedStack> stacks;;
}
package mods.neiplugins.lists;

import java.util.List;

public class SimpleListActiveElement extends SimpleListElement implements IActiveListElement
{
	public boolean active;
	public SimpleListActiveElement(String title, boolean active)
	{
		super(title);
		this.active = active;
	}
	@Override
	public boolean isActive()
	{
		return this.active;
	}
}
package mods.neiplugins.lists;

import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.item.ItemStack;

import codechicken.nei.PositionedStack;
import codechicken.nei.forge.GuiContainerManager;

public interface IListElement
{
	public void draw(GuiContainerManager gui, Dimension size);
	public boolean click(int button);
	public List<String> handleTooltip(GuiList gui, List<String> currenttip, Point mousepos);
	public List<String> handleItemTooltip(GuiList gui, ItemStack stack, List<String> currenttip, Point mousepos);
	public ArrayList<PositionedStack> getStacks();
}
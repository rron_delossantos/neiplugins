package mods.neiplugins.lists;

import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.item.ItemStack;

import codechicken.nei.PositionedStack;
import codechicken.nei.forge.GuiContainerManager;

public class SimpleListElement implements IListElement
{
	public SimpleListElement(String title)
	{
		this.title = title;
	}
	public void draw(GuiContainerManager gui, Dimension size)
	{
		gui.drawTextCentered(this.getTitle(), size.width / 2, (size.height - 8) / 2, 14737632, false);
	}
	public boolean click(int button)
	{
		return false;
	}
	public List<String> handleTooltip(GuiList gui, List<String> currenttip, Point mousepos)
	{
		return currenttip;
	}

	public List<String> handleItemTooltip(GuiList gui, ItemStack stack, List<String> currenttip, Point mousepos)
	{
		return currenttip;
	}

	public ArrayList<PositionedStack> getStacks() {
		return null;
	}

	public String getTitle()
	{
		return title;
	}

	protected String title;
}
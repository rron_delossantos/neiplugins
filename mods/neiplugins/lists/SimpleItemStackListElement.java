package mods.neiplugins.lists;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.item.ItemStack;

import codechicken.nei.PositionedStack;
import codechicken.nei.forge.GuiContainerManager;

public class SimpleItemStackListElement implements IListElement
{
	public SimpleItemStackListElement(String title, ItemStack stack)
	{
		this.title = title;
		this.stacks = new ArrayList<PositionedStack>();
		if (stack != null)
			this.stacks.add(new PositionedStack(stack,2,2));
	}
	public void draw(GuiContainerManager gui, Dimension size)
	{
		String s = this.title;
		if (s.length() > 20)
			s = s.substring(0,17).concat("...");
		gui.drawText(20, 6, s);
	}
	public boolean click(int button)
	{
		return false;
	}
	public List<String> handleTooltip(GuiList gui, List<String> currenttip, Point mousePos)
	{
		if (!stackRect.contains(mousePos))
			currenttip.add(this.title);
		return currenttip;
	}

	public List<String> handleItemTooltip(GuiList gui, ItemStack stack, List<String> currenttip, Point mousepos)
	{
		return currenttip;
	}

	public ArrayList<PositionedStack> getStacks() {
		return stacks;
	}

	protected String title;
	public ArrayList<PositionedStack> stacks;
	public static Rectangle stackRect = new Rectangle(1,1,18,18);

}